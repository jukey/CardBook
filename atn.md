### 1. Title
cs: CardBook<br>
da: CardBook<br>
de: CardBook<br>
el: Επαφές (CardBook)<br>
en-US: **CardBook**<br>
es: CardBook<br>
fr: CardBook<br>
hr: <br>
hu: Névjegyzék (CardBook)<br>
id: Kontak (CardBook)<br>
it: CardBook<br>
ja: 連絡先「CardBook」<br>
ko: 연락처 (CardBook)<br>
lt: <br>
nl: CardBook<br>
pl: CardBook<br>
pt-BR: CardBook<br>
pt-PT: CardBook<br>
ro: Contacte (CardBook)<br>
ru: CardBook<br>
sl: CardBook<br>
sv-SE: Kontakter (CardBook)<br>
uk: Контакти «CardBook»<br>
vi: <br>
zh-CN: CardBook<br>


### 2. Summary
cs: Nový adresář pro Thunderbird založený na standardech CardDAV a vCard.<br>
da: En ny Thunderbird-addressebog baseret på CardDAV- og vCard-standarderne.<br>
de: Ein neues Adressbuch für Thunderbird nach CardDAV und VCARD standard.<br>
el: Ένα νέο βιβλίο διευθύνσεων Thunderbird που βασίζεται στα πρότυπα CardDAV και εικονική κάρτα (vCard).<br>
en-US: **A new Thunderbird address book based on the CardDAV and vCard standards.**<br>
es: Un nuevo directorio de contactos para Thunderbird, basado en los estándares CardDAV y vCard.<br>
fr: Un nouveau carnet d'adresses pour Thunderbird basé sur les standards vCard et CardDAV.<br>
hr: <br>
hu: Névjegyzék (CardBook) alapul a CardDAV és a vCard szabványoknak.<br>
id: Buku alamat Thunderbird baru berdasarkan standar KartuDAV dan vKartu.<br>
it: Una nuova rubrica di Thunderbird basata sugli standard CardDAV e vCard.<br>
ja: カード分散オーサリングとバージョン管理「CardDAV」および仮想カード「vCard」標準に基づく新しいThunderbirdアドレス帳<br>
ko: 카드 분산 저작 및 버전 관리(CardDAV) 및 가상 카드(vCard) 표준에 기반한 새로운 썬더버드(Thunderbird) 주소록입니다.<br>
lt: <br>
nl: Een nieuw adresboek voor gebruik in Thunderbird, gebaseerd op de standaards CardDav en vCard.<br>
pl: Nowa książka adresowa do Thunderbirda oparta na standardach CardDAV i vCard.<br>
pt-BR: Uma nova agenda.<br>
pt-PT: Uma nova agenda para o Thunderbird construída com os standards CardDAV e vCard.<br>
ro: O nouă adresă Thunderbird bazată pe standardele CardDAV și vCard.<br>
ru: Новая адресная книга для Thunderbird, основанная на стандартах CardDAV и
vCard.<br>
sl: Nov imenik za Thunderbird, ki je osnovan na standardih CardDAV in vCard.<br>
sv-SE: En ny Thunderbird adressbok baserad på CardDAV- och vCard-standarden.<br>
uk: Нова адресна книга Thunderbird на основі стандартів CardDAV та vCard.<br>
vi: Một cuốn sổ địa chỉ mới của Thunderbird dựa trên các tiêu chuẩn CardDAV và Thẻ Ảo (vCard).<br>
zh-CN: 一个新的基于CardDAV和vCard标准的Thunderbird通讯录。<br>

### 3. Image Captions

#### a) https://addons.thunderbird.net/user-media/previews/full/205/205504.png?modified=1542792537<br>
https://transvision.mozfr.org/?recherche=Classic+View&repo=gecko_strings&sourcelocale=en-US&locale=hu&search_type=strings<br>
cs: Klasický pohled<br>
da: Klassisk udseende<br>
de: Klassische Ansicht<br>
el: Κλασσική προβολή<br>
en-US: **Classic View**<br>
es-ES: Vista clásica<br>
fr: Vue classique<br>
hr: Klasični pogled<br>
hu: Klasszikus nézet<br>
id: Tampilan Klasik<br>
it: Visualizzazione classica<br>
ja: クラシック表示<br>
ko: 기본 보기<br>
lt: Numatytasis<br>
nl: Klassiek beeld<br>
pt-BR: Clássico<br>
pt-PT: Clássico<br>
ro: Vedere clasică<br>
ru: Классический вид<br>
sl: Običajna<br>
sv-SE: Klassisk vy<br>
uk: Класичне<br>
vi: Hiển thị Cổ điển<br>
zh-CN: 经典视图<br>

#### b) https://addons.thunderbird.net/user-media/previews/full/205/205505.png?modified=1542792537<br>
https://transvision.mozfr.org/?recherche=Vertical+View&repo=gecko_strings&sourcelocale=en-US&locale=hu&search_type=strings<br>
cs: Svislý pohled<br>
da: Lodret udseende<br>
de: Vertikale Ansicht<br>
el: Κάθετη προβολή<br>
en-US: **Vertical View**<br>
es-ES: Vista vertical<br>
fr: Vue verticale<br>
hr: Okomiti pogled<br>
hu: Függőleges nézet<br>
id: Tampilan Tegak<br>
it: Visualizzazione verticale<br>
ja: 縦表示<br>
ko: 아래로 보기<br>
lt: Vertikalus<br>
nl: Verticaal beeld<br>
pl: Pionowy<br>
pt-BR: Vertical<br>
pt-PT: Vertical<br>
ro: Vedere verticală<br>
ru: Вертикальный вид<br>
sl: Navpična<br>
sv-SE: Vertikal vy<br>
uk: Вертикальне<br>
vi: Hiển thị Dọc<br>
zh-CN: 直视图<br>

#### c) https://addons.thunderbird.net/user-media/previews/full/205/205506.png?modified=1542792537<br>
https://transvision.mozfr.org/?recherche=autocompletion&repo=gecko_strings&sourcelocale=en-US&locale=hu&search_type=strings<br>
https://transvision.mozfr.org/?recherche=and&repo=gecko_strings&sourcelocale=en-US&locale=hu&search_type=strings&entire_string=entire_string<br>
https://transvision.mozfr.org/?recherche=Contacts+Sidebar&repo=gecko_strings&sourcelocale=en-US&locale=hu&search_type=strings<br>
cs: Automatické doplňování adres a Lišta kontaktů<br>
da: Autofuldførelse for adresser og Kontakter<br>
de: Adress-Autovervollständigung und Kontakte-Sidebar<br>
el: Αυτόματη συμπλήρωση διεύθυνσης και Πλευρική στήλη επαφών<br>
en-US: **Address Autocompletion and Contacts Sidebar**<br>
es-ES: Autocompletado de direcciones y Panel lateral de contactos<br>
fr: Suggestion des emails et barre de contacts latérale<br>
hr: Automatsko dovršavanje adrese i Bočna traka kontakata<br>
hu: Automatikus címkiegészítés és Kapcsolatok oldalsáv<br>
id: Otomatis Melengkapi Alamat dan Bilah Samping Daftar Kenalan<br>
it: Completamento automatico indirizzi e Barra contatti<br>
ja: アドレスの自動補完 と アドレスサイドバー<br>
ko: 메일 주소 자동 완성 그리고 주소록 탐색창<br>
lt: Adreso užbaigimas pagal surinktą jo pradžią ir Parankinė (adresatai)<br>
nl: Automatische adresaanvulling en Adressenzijbalk<br>
pl: Automatyczne uzupełnianie adresu oraz Panel adresów<br>
pt-BR: Autocompletar endereços e Painel de contatos<br>
pt-PT: Conclusão automática de endereços e Painel de contactos<br>
ro: Completarea automată a adresei și Listă de contacte<br>
ru: Автодополнение адресов и Панель контактов<br>
sl: Samodokončanje naslova in Stranska vrstica stikov<br>
sv-SE: Automatisk komplettering av e-postadresser och Kontaktsidofältet<br>
uk: Автозавершення адреси та Панель контактів<br>
vi: Tự động điền Địa chỉ và Thanh Lề Danh Bạ<br>
zh-CN: 地址自动完成 并且 联系人侧栏<br>

#### d) https://addons.thunderbird.net/user-media/previews/full/205/205507.png?modified=1542792537
https://transvision.mozfr.org/?recherche=Address+Autocompletion&repo=gecko_strings&sourcelocale=en-US&locale=hu&search_type=strings<br>
cs: <br>
da: <br>
de: <br>
el: <br>
en-US: **Address Autocompletion in Lightning**<br>
es-ES: <br>
fr: Suggestion des contacts pour Lightning<br>
hr: <br>
hu: Automatikus címkiegészítés Lightningban<br>
id: <br>
it: <br>
ja: <br>
ko: <br>
lt: <br>
nl: <br>
pl: <br>
pt-BR: <br>
pt-PT: <br>
ro: <br>
ru: <br>
sl: <br>
sv-SE: <br>
uk: <br>
vi: <br>
zh-CN: <br>

#### e) https://addons.thunderbird.net/user-media/previews/full/205/205508.png?modified=1542792537
cs: <br>
da: <br>
de: <br>
el: <br>
en-US: **Search for Duplicate Contacts**<br>
es-ES: <br>
fr: Recherche des contacts dupliqués<br>
hr: <br>
hu: Ismételt névjegyék keresése<br>
id: <br>
it: <br>
ja: <br>
ko: <br>
lt: <br>
nl: <br>
pl: <br>
pt-BR: <br>
pt-PT: <br>
ro: <br>
ru: <br>
sl: <br>
sv-SE: <br>
uk: <br>
vi: <br>
zh-CN: <br>

#### f) https://addons.thunderbird.net/user-media/previews/full/205/205509.png?modified=1542792537
cs: <br>
da: <br>
de: <br>
el: <br>
en-US: **Merge Contacts**<br>
es-ES: <br>
fr: Fusion de contacts<br>
hr: <br>
hu: Névjegyek egyesítése<br>
id: <br>
it: <br>
ja: <br>
ko: <br>
lt: <br>
nl: <br>
pl: <br>
pt-BR: <br>
pt-PT: <br>
ro: <br>
ru: <br>
sl: <br>
sv-SE: <br>
uk: <br>
vi: <br>
zh-CN: <br>

#### g) https://addons.thunderbird.net/user-media/previews/full/205/205510.png?modified=1542792537
https://transvision.mozfr.org/?recherche=Message+Header&repo=gecko_strings&sourcelocale=en-US&locale=hu&search_type=strings<br>
cs: <br>
da: <br>
de: <br>
el: <br>
en-US: **Manage Contacts from the Message Header**<br>
es-ES: <br>
fr: Gestion des contacts depuis l'écran principal<br>
hr: <br>
hu: Névjegyek kezelése az üzenetfejlécéből<br>
id: <br>
it: <br>
ja: <br>
ko: <br>
lt: <br>
nl: <br>
pl: <br>
pt-BR: <br>
pt-PT: <br>
ro: <br>
ru: <br>
sl: <br>
sv-SE: <br>
uk: <br>
vi: <br>
zh-CN: <br>

#### h) https://addons.thunderbird.net/user-media/previews/full/205/205511.png?modified=1542792537
https://transvision.mozfr.org/?recherche=Filters&repo=gecko_strings&sourcelocale=en-US&locale=hu&search_type=strings&entire_string=entire_string<br>
cs: Filtry<br>
da: Filtre<br>
de: Filter<br>
el: Φίλτρα<br>
en-US: **Filters**<br>
es-ES: Filtros<br>
fr: Filtres<br>
hr: Filteri<br>
hu: Szűrők<br>
id: Filter<br>
it: Filtri<br>
ja: フィルター<br>
ko: 필터<br>
lt: Filtrus<br>
nl: Filters<br>
pl: Filtry<br>
pt-BR: Filtros<br>
pt-PT: Filtros<br>
ro: Filtre<br>
ru: Фильтры<br>
sl: Filtri<br>
sv-SE: Filter<br>
uk: Фільтри<br>
vi: Bộ lọc<br>
zh-CN: 过滤器<br>

#### i) https://addons.thunderbird.net/user-media/previews/full/205/205512.png?modified=1542792537
https://transvision.mozfr.org/?recherche=Anniversary&repo=gecko_strings&sourcelocale=en-US&locale=hu&search_type=strings<br>
https://transvision.mozfr.org/?recherche=Default+Reminder&repo=gecko_strings&sourcelocale=en-US&locale=hu&search_type=strings<br>
cs: <br>
da: <br>
de: <br>
el: <br>
en-US: **Anniversary Reminder**<br>
es-ES: <br>
fr: Liste des anniversaires<br>
hr: <br>
hu: Évforduló emlékeztető<br>
id: <br>
it: <br>
ja: <br>
ko: <br>
lt: <br>
nl: <br>
pl: <br>
pt-BR: <br>
pt-PT: <br>
ro: <br>
ru: <br>
sl: <br>
sv-SE: <br>
uk: <br>
vi: <br>
zh-CN: <br>

#### j) https://addons.thunderbird.net/user-media/previews/full/205/205513.png?modified=1542792537
cs: <br>
da: <br>
de: <br>
el: <br>
en-US: **Create Anniversary Events in Lightning**<br>
es-ES: <br>
fr: Export des anniversaires dans Lightning<br>
hr: <br>
hu: Évfordulói események létrehozása Lightningban<br>
id: <br>
it: <br>
ja: <br>
ko: <br>
lt: <br>
nl: <br>
pl: <br>
pt-BR: <br>
pt-PT: <br>
ro: <br>
ru: <br>
sl: <br>
sv-SE: <br>
uk: <br>
vi: <br>
zh-CN: <br>

#### k) https://addons.thunderbird.net/user-media/previews/full/205/205514.png?modified=1542792537
cs: <br>
da: <br>
de: <br>
el: <br>
en-US: **Import and Export Files (CSV, VCF)**<br>
es-ES: <br>
fr: Import et export des fichiers (CSV, VCF)<br>
hr: <br>
hu: Fájlok importálása és exportálása (CSV, VCF)<br>
id: <br>
it: <br>
ja: <br>
ko: <br>
lt: <br>
nl: <br>
pl: <br>
pt-BR: <br>
pt-PT: <br>
ro: <br>
ru: <br>
sl: <br>
sv-SE: <br>
uk: <br>
vi: <br>
zh-CN: <br>

#### l) https://addons.thunderbird.net/user-media/previews/full/205/205515.png?modified=1542792537
cs: <br>
da: <br>
de: <br>
el: <br>
en-US: **Standalone CardBook (thunderbird.exe -cardbook)**<br>
es-ES: <br>
fr: Utilisation de CardBook comme un programme indépendant (thunderbird.exe -cardbook)<br>
hr: <br>
hu: Önálló CardBook (thunderbird.exe -cardbook)<br>
id: <br>
it: <br>
ja: <br>
ko: <br>
lt: <br>
nl: <br>
pl: <br>
pt-BR: <br>
pt-PT: <br>
ro: <br>
ru: <br>
sl: <br>
sv-SE: <br>
uk: <br>
vi: <br>
zh-CN: <br>

### 4. About this Add-on
cs: <br>
da: <br>
de: <br>
el: <br>
en-US: <br>
es-ES: <br>
fr: <br>
hr: <br>
hu: <br>
id: <br>
it: <br>
ja: <br>
ko: <br>
lt: <br>
nl: <br>
pl: <br>
pt-BR: <br>
pt-PT: <br>
ro: <br>
ru: <br>
sl: <br>
sv-SE: <br>
uk: <br>
vi: <br>
zh-CN: <br>
