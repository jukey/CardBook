if ("undefined" == typeof(cardBookObserver)) {
	try {
		ChromeUtils.import("resource://gre/modules/Services.jsm");
	}
	catch(e) {
		Components.utils.import("resource://gre/modules/Services.jsm");
	}

	var cardBookObserver = {
		register: function() {
			cardBookObserverRepository.registerAll(this);
		},
		
		unregister: function() {
			cardBookObserverRepository.unregisterAll(this);
		},
		
		observe: function(aSubject, aTopic, aData) {
			switch (aTopic) {
				case "cardbook.DBOpen":
					cardbookSynchronization.loadComplexSearchAccounts();
					break;
				case "cardbook.complexSearchInitLoaded":
					cardbookSynchronization.loadAccounts();
					break;
				case "cardbook.cardCreated":
				case "cardbook.cardModified":
				case "cardbook.cardsDeleted":
				case "cardbook.cardsDragged":
				case "cardbook.cardsMerged":
				case "cardbook.cardsImportedFromDir":
				case "cardbook.cardsImportedFromFile":
				case "cardbook.cardsPasted":
				case "cardbook.categoryConvertedToList":
				case "cardbook.categoryCreated":
				case "cardbook.categoryDeleted":
				case "cardbook.categoryRenamed":
				case "cardbook.categorySelected":
				case "cardbook.categoryUnselected":
				case "cardbook.displayNameGenerated":
				case "cardbook.emailCollectedByFilter":
				case "cardbook.emailDeletedByFilter":
				case "cardbook.listConvertedToCategory":
				case "cardbook.outgoingEmailCollected":
				case "cardbook.redoActionDone":
				case "cardbook.undoActionDone":
					// for the yellow star
					if (!("undefined" == typeof(ReloadMessage))) {
						ReloadMessage();
					}
					break;
			}
		}
	};
};
