if ("undefined" == typeof(cardbookActions)) {
	try {
		ChromeUtils.import("chrome://cardbook/content/cardbookRepository.js");
	}
	catch(e) {
		Components.utils.import("chrome://cardbook/content/cardbookRepository.js");
	}

	var cardbookActions = {

		lTimerActionAll: {},
		syncActivity: {},

		setUndoAndRedoMenuAndButton: function () {
			var myCurrentUndoId = cardbookRepository.currentUndoId;
			cardbookIndexedDB.setUndoAndRedoMenuAndButton("menu_undo", "cardbookToolbarBackButton", myCurrentUndoId);
			myCurrentUndoId++;
			cardbookIndexedDB.setUndoAndRedoMenuAndButton("menu_redo", "cardbookToolbarForwardButton", myCurrentUndoId);
		},

		saveCurrentUndoId: function () {
			cardbookPreferences.setStringPref("extensions.cardbook.currentUndoId", cardbookRepository.currentUndoId);
		},

		addUndoCardsAction: function (aActionCode, aActionMessage, aOldCards, aNewCards) {
			var myNextUndoId = cardbookRepository.currentUndoId + 1;
			cardbookIndexedDB.addUndoItem(myNextUndoId, aActionCode, aActionMessage, aOldCards, aNewCards);
		},

		undo: function () {
			cardbookIndexedDB.executeUndoItem();
		},

		redo: function () {
			cardbookIndexedDB.executeRedoItem();
		},


		initSyncActivity: function(aDirPrefId, aDirPrefName) {
			var gActivityManager = Components.classes["@mozilla.org/activity-manager;1"].getService(Components.interfaces.nsIActivityManager);
			var process = Components.classes["@mozilla.org/activity-process;1"].createInstance(Components.interfaces.nsIActivityProcess);
			
			var processName = cardbookRepository.strBundle.formatStringFromName("synchroRunning", [aDirPrefName], 1);

			process.init(processName, "CardBook");
			process.iconClass = "syncMail";
			process.groupingStyle = Components.interfaces.nsIActivity.GROUPING_STYLE_BYCONTEXT;
			process.contextDisplayText = aDirPrefName;
			process.contextType = aDirPrefId;
			
			gActivityManager.addActivity(process);

			if (!(cardbookActions.syncActivity[aDirPrefId] != null && cardbookActions.syncActivity[aDirPrefId] !== undefined && cardbookActions.syncActivity[aDirPrefId] != "")) {
				cardbookActions.syncActivity[aDirPrefId] = {};
			}
			cardbookActions.syncActivity[aDirPrefId].syncProcess = process;
		},
		
		fetchSyncActivity: function(aDirPrefId, aCountDone, aCountTotal) {
			if (cardbookActions.syncActivity[aDirPrefId] && cardbookActions.syncActivity[aDirPrefId].syncProcess) {
				var processMessage = cardbookRepository.strBundle.formatStringFromName("synchroProcessed", [aCountDone, aCountTotal], 2);
				cardbookActions.syncActivity[aDirPrefId].syncProcess.setProgress(processMessage, aCountDone, aCountTotal);
			}
		},
		
		finishSyncActivity: function(aDirPrefId, aDirPrefName) {
			if (cardbookActions.syncActivity[aDirPrefId] && cardbookActions.syncActivity[aDirPrefId].syncProcess) {
				var gActivityManager = Components.classes["@mozilla.org/activity-manager;1"].getService(Components.interfaces.nsIActivityManager);
				cardbookActions.syncActivity[aDirPrefId].syncProcess.state = Components.interfaces.nsIActivityProcess.STATE_COMPLETED;
				gActivityManager.removeActivity(cardbookActions.syncActivity[aDirPrefId].syncProcess.id);
			}
		},
			
		finishSyncActivityOK: function(aDirPrefId, aDirPrefName) {
			var gActivityManager = Components.classes["@mozilla.org/activity-manager;1"].getService(Components.interfaces.nsIActivityManager);
			cardbookActions.finishSyncActivity(aDirPrefId, aDirPrefName);
			if (cardbookActions.syncActivity[aDirPrefId].syncEvent) {
				if (gActivityManager.containsActivity(cardbookActions.syncActivity[aDirPrefId].syncEvent.id)) {
					gActivityManager.removeActivity(cardbookActions.syncActivity[aDirPrefId].syncEvent.id);
				}
			}
			var eventName = cardbookRepository.strBundle.formatStringFromName("synchroFinished", [aDirPrefName], 1);
			var event = Components.classes["@mozilla.org/activity-event;1"].createInstance(Components.interfaces.nsIActivityEvent);
			event.init(eventName, null, "", null, Date.now());
			event.iconClass = "syncMail";
			gActivityManager.addActivity(event);
			cardbookActions.syncActivity[aDirPrefId].syncEvent = event;
		},
			
		addActivity: function(aMessageCode, aArray, aIcon) {
			var gActivityManager = Components.classes["@mozilla.org/activity-manager;1"].getService(Components.interfaces.nsIActivityManager);
			var eventName = cardbookRepository.strBundle.formatStringFromName(aMessageCode, aArray, aArray.length);
			var event = Components.classes["@mozilla.org/activity-event;1"].createInstance(Components.interfaces.nsIActivityEvent);
			event.init(eventName, null, "", null, Date.now());
			event.iconClass = aIcon;// deleteMail addItem editItem
			gActivityManager.addActivity(event);
		},
			
		addActivityFromUndo: function(aActionId) {
			if (cardbookRepository.currentAction[aActionId]) {
				var gActivityManager = Components.classes["@mozilla.org/activity-manager;1"].getService(Components.interfaces.nsIActivityManager);
				var event = Components.classes["@mozilla.org/activity-event;1"].createInstance(Components.interfaces.nsIActivityEvent);
				event.init(cardbookRepository.currentAction[aActionId].message, null, "", null, Date.now());
				switch(cardbookRepository.currentAction[aActionId].actionCode) {
					case "cardsConverted":
					case "cardsMerged":
					case "cardsDuplicated":
					case "displayNameGenerated":
					case "cardModified":
					case "categoryRenamed":
					case "categoryConvertedToList":
					case "listConvertedToCategory":
						event.iconClass = "editItem";
						break;
					case "outgoingEmailCollected":
					case "emailCollectedByFilter":
					case "emailDeletedByFilter":
					case "cardsImportedFromFile":
					case "cardsImportedFromDir":
					case "cardCreated":
					case "categoryCreated":
					case "categorySelected":
						event.iconClass = "addItem";
						break;
					case "cardsPasted":
					case "cardsDragged":
					case "linePasted":
						event.iconClass = "moveMail";
						break;
					case "undoActionDone":
					case "redoActionDone":
						event.iconClass = "undo";
						break;
					case "categoryUnselected":
					case "categoryDeleted":
					case "cardsDeleted":
						event.iconClass = "deleteMail";
						break;
				}
				gActivityManager.addActivity(event);
			}
		},
			
		addUndoMessage: function(aActionId, aArray) {
			if (cardbookRepository.currentAction[aActionId]) {
				var myActionCode = cardbookRepository.currentAction[aActionId].actionCode;
				switch(myActionCode) {
					case "cardsConverted":
					case "outgoingEmailCollected":
					case "emailCollectedByFilter":
					case "emailDeletedByFilter":
					case "cardsMerged":
					case "cardsDuplicated":
					case "cardsPasted":
					case "cardsDragged":
					case "displayNameGenerated":
					case "linePasted":
						cardbookRepository.currentAction[aActionId].message = cardbookRepository.strBundle.GetStringFromName(myActionCode + 'Undo');
						break;
					case "cardsImportedFromFile":
					case "cardsImportedFromDir":
					case "undoActionDone":
					case "redoActionDone":
					case "cardCreated":
					case "cardModified":
					case "categoryCreated":
					case "categoryRenamed":
					case "categorySelected":
					case "categoryUnselected":
					case "categoryDeleted":
					case "categoryConvertedToList":
					case "listConvertedToCategory":
						cardbookRepository.currentAction[aActionId].message = cardbookRepository.strBundle.formatStringFromName(myActionCode + 'Undo', aArray, aArray.length);
						break;
					case "cardsDeleted":
						if (aArray && aArray.length == 1) {
							cardbookRepository.currentAction[aActionId].message = cardbookRepository.strBundle.formatStringFromName(myActionCode + '1' + 'Undo', [aArray[0].fn], 1);
						} else {
							cardbookRepository.currentAction[aActionId].message = cardbookRepository.strBundle.GetStringFromName(myActionCode + '2' + 'Undo');
						}
						break;
				}
			}
		},

		startAction: function (aActionCode, aArray, aRefreshAccount) {
			cardbookRepository.currentActionId++;
			if (!cardbookRepository.currentAction[cardbookRepository.currentActionId]) {
				cardbookRepository.currentAction[cardbookRepository.currentActionId] = {actionCode : aActionCode, message : "", oldCards: [], newCards: [], total: 0, done: 0, files: [], refresh: ""};
			}
			if (aRefreshAccount != null && aRefreshAccount !== undefined && aRefreshAccount != "") {
				cardbookRepository.currentAction[cardbookRepository.currentActionId].refresh = aRefreshAccount;
			}
			cardbookActions.addUndoMessage(cardbookRepository.currentActionId, aArray);
			return cardbookRepository.currentActionId;
		},

		endAsyncAction: function (aActionId) {
			if (cardbookRepository.currentAction[aActionId]) {
				cardbookActions.lTimerActionAll[aActionId] = Components.classes["@mozilla.org/timer;1"].createInstance(Components.interfaces.nsITimer);
				var lTimerActions = cardbookActions.lTimerActionAll[aActionId];
				lTimerActions.initWithCallback({ notify: function(lTimerActions) {
					var myAction = cardbookRepository.currentAction[aActionId];
					if (myAction.total == myAction.done) {
						cardbookActions.endAction(aActionId);
						lTimerActions.cancel();
					}
				}
				}, 1000, Components.interfaces.nsITimer.TYPE_REPEATING_SLACK);
			}
		},

		endAction: function (aActionId) {
			if (cardbookRepository.currentAction[aActionId]) {
				var myAction = cardbookRepository.currentAction[aActionId];
				if (myAction.files.length > 0) {
					cardbookRepository.reWriteFiles(myAction.files);
					cardbookActions.addActivityFromUndo(aActionId);
					if (myAction.actionCode != "undoActionDone" && myAction.actionCode != "redoActionDone") {
						cardbookActions.addUndoCardsAction(myAction.actionCode, myAction.message, myAction.oldCards, myAction.newCards);
					}
					if (myAction.refresh != "") {
						cardbookUtils.notifyObservers(myAction.actionCode, "force::" + myAction.refresh);
					} else {
						cardbookUtils.notifyObservers(myAction.actionCode);
					}
				}
			}
		}

	};
};
