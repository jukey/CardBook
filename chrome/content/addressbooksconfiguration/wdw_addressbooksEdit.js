if ("undefined" == typeof(wdw_addressbooksEdit)) {
	try {
		ChromeUtils.import("chrome://cardbook/content/cardbookRepository.js");
	}
	catch(e) {
		Components.utils.import("chrome://cardbook/content/cardbookRepository.js");
	}

	var wdw_addressbooksEdit = {
		
		initialDateFormat: "",

		convertVCards: function () {
			let myTopic = "cardsConverted";
			let myActionId = cardbookActions.startAction(myTopic);
			let myCardList = JSON.parse(JSON.stringify(cardbookRepository.cardbookDisplayCards[window.arguments[0].dirPrefId].cards));
			let myTargetVersion = cardbookPreferences.getVCardVersion(window.arguments[0].dirPrefId);
			let myTargetName = cardbookPreferences.getName(window.arguments[0].dirPrefId);
			let myTargetABType = cardbookPreferences.getType(window.arguments[0].dirPrefId);
			// the date format is no longer stored
			let myNewDateFormat = cardbookRepository.getDateFormat(window.arguments[0].dirPrefId);
			let counter = 0;
			for (let i = 0; i < myCardList.length; i++) {
				let myCard = myCardList[i];
				
				let myTempCard = new cardbookCardParser();
				cardbookUtils.cloneCard(myCard, myTempCard);
				if (cardbookUtils.convertVCard(myTempCard, myTargetName, myTargetVersion, wdw_addressbooksEdit.initialDateFormat, myNewDateFormat, myTargetABType, myTargetABType)) {
					cardbookRepository.saveCard(myCard, myTempCard, myActionId, false);
					counter++;
				}
			}
			cardbookRepository.writePossibleCustomFields();
			wdw_addressbooksEdit.deleteOldDateFormat();
			document.getElementById("convertVCardsLabel").setAttribute('hidden', 'true');
			cardbookUtils.formatStringForOutput(myTopic, [myTargetName, myTargetVersion, counter]);
			cardbookActions.endAction(myActionId);
		},
		
		loadFnFormula: function () {
			document.getElementById("fnFormulaTextBox").value = cardbookPreferences.getFnFormula(window.arguments[0].dirPrefId);
			var orgStructure = cardbookPreferences.getStringPref("extensions.cardbook.orgStructure");
			if (orgStructure != "") {
				var allOrg = cardbookUtils.unescapeArray(cardbookUtils.escapeString(orgStructure).split(";"));
			} else {
				var allOrg = [];
			}
			var myLabel = "";
			myLabel = myLabel + "{{1}} : " + cardbookRepository.strBundle.GetStringFromName("prefixnameLabel") + "    ";
			myLabel = myLabel + "{{2}} : " + cardbookRepository.strBundle.GetStringFromName("firstnameLabel") + "    ";
			myLabel = myLabel + "{{3}} : " + cardbookRepository.strBundle.GetStringFromName("othernameLabel") + "    ";
			myLabel = myLabel + "{{4}} : " + cardbookRepository.strBundle.GetStringFromName("lastnameLabel");
			document.getElementById('fnFormulaDescriptionLabel1').value = myLabel.trim();
			myLabel = "";
			myLabel = myLabel + "{{5}} : " + cardbookRepository.strBundle.GetStringFromName("suffixnameLabel") + "    ";
			myLabel = myLabel + "{{6}} : " + cardbookRepository.strBundle.GetStringFromName("nicknameLabel") + "    ";
			var count = 7;
			if (allOrg.length === 0) {
				myLabel = myLabel + "{{" + count + "}} : " + cardbookRepository.strBundle.GetStringFromName("orgLabel");
				count++;
			} else {
				for (var i = 0; i < allOrg.length; i++) {
					myLabel = myLabel + "{{" + count + "}} : " + allOrg[i] + "    ";
					count++;
				}
			}
			document.getElementById('fnFormulaDescriptionLabel2').value = myLabel.trim();
			myLabel = "";
			myLabel = myLabel + "{{" + count + "}} : " + cardbookRepository.strBundle.GetStringFromName("titleLabel") + "    ";
			count++;
			myLabel = myLabel + "{{" + count + "}} : " + cardbookRepository.strBundle.GetStringFromName("roleLabel") + "    ";
			document.getElementById('fnFormulaDescriptionLabel3').value = myLabel.trim();
		},

		populateApplyToAB: function () {
			let applyToABMenupopup = document.getElementById('applyToABMenupopup');
			let applyToABButton = document.getElementById('applyToABButton');
			cardbookElementTools.loadAddressBooks(applyToABMenupopup, applyToABButton, "", true, true, true, false, true);
		},

		applyApplyToAB: function (aEvent) {
			if (aEvent.target && aEvent.target.value) {
				let myDirPrefId = aEvent.target.value;
				for (var i = 0; i < cardbookRepository.cardbookAccounts.length; i++) {
					if (cardbookRepository.cardbookAccounts[i][1]) {
						if ((cardbookRepository.cardbookAccounts[i][4] == myDirPrefId) || ("allAddressBooks" == myDirPrefId)) {
							cardbookPreferences.setFnFormula(cardbookRepository.cardbookAccounts[i][4], document.getElementById('fnFormulaTextBox').value);
						}
					}
				}
			}
		},

		resetFnFormula: function () {
			document.getElementById('fnFormulaTextBox').value = cardbookRepository.defaultFnFormula;
		},

		showAutoSyncInterval: function () {
			if (document.getElementById('autoSyncCheckBox').checked) {
				document.getElementById('autoSyncInterval').disabled = false;
				document.getElementById('autoSyncIntervalTextBox').disabled = false;
			} else {
				document.getElementById('autoSyncInterval').disabled = true;
				document.getElementById('autoSyncIntervalTextBox').disabled = true;
			}
		},

		load: function () {
			wdw_addressbooksEdit.initialDateFormat = cardbookPreferences.getDateFormat(window.arguments[0].dirPrefId);

			document.getElementById("colorInput").value = cardbookPreferences.getColor(window.arguments[0].dirPrefId);
			document.getElementById("nameTextBox").value = cardbookPreferences.getName(window.arguments[0].dirPrefId);
			document.getElementById("typeTextBox").value = cardbookPreferences.getType(window.arguments[0].dirPrefId);
			document.getElementById("urlTextBox").value = cardbookPreferences.getUrl(window.arguments[0].dirPrefId);
			document.getElementById("usernameTextBox").value = cardbookPreferences.getUser(window.arguments[0].dirPrefId);
			document.getElementById("readonlyCheckBox").setAttribute('checked', cardbookPreferences.getReadOnly(window.arguments[0].dirPrefId));
			document.getElementById("vCardVersionTextBox").value = cardbookPreferences.getVCardVersion(window.arguments[0].dirPrefId);
			document.getElementById("urnuuidCheckBox").setAttribute('checked', cardbookPreferences.getUrnuuid(window.arguments[0].dirPrefId));

			if (cardbookUtils.isMyAccountRemote(document.getElementById("typeTextBox").value)) {
				document.getElementById('syncTab').setAttribute("collapsed", false);
				document.getElementById("autoSyncCheckBox").setAttribute('checked', cardbookPreferences.getAutoSyncEnabled(window.arguments[0].dirPrefId));
				document.getElementById("autoSyncIntervalTextBox").value = cardbookPreferences.getAutoSyncInterval(window.arguments[0].dirPrefId);
				wdw_addressbooksEdit.showAutoSyncInterval();
			} else {
				document.getElementById('syncTab').setAttribute("collapsed", true);
			}
			
			wdw_addressbooksEdit.loadFnFormula();
			wdw_addressbooksEdit.searchForWrongCards();
			wdw_addressbooksEdit.populateApplyToAB();
		},

		deleteOldDateFormat: function () {
			try {
				Services.prefs.deleteBranch(cardbookPreferences.prefCardBookData + window.arguments[0].dirPrefId + "." + "dateFormat");
			}
			catch(e) {}
		},

		searchForWrongCards: function () {
			Services.tm.currentThread.dispatch({ run: function() {
				// the date format is no longer stored
				let myNewDateFormat = cardbookRepository.getDateFormat(window.arguments[0].dirPrefId);
				if (wdw_addressbooksEdit.initialDateFormat != myNewDateFormat) {
					document.getElementById("convertVCardsLabel").removeAttribute('hidden');
				} else {
					let myVersion = cardbookPreferences.getVCardVersion(window.arguments[0].dirPrefId);
					for (let i = 0; i < cardbookRepository.cardbookDisplayCards[window.arguments[0].dirPrefId].cards.length; i++) {
						let myCard = cardbookRepository.cardbookDisplayCards[window.arguments[0].dirPrefId].cards[i];
						if (myCard.version != myVersion) {
							document.getElementById("convertVCardsLabel").removeAttribute('hidden');
							break;
						}
					}
					wdw_addressbooksEdit.deleteOldDateFormat();
				}
			}}, Components.interfaces.nsIEventTarget.DISPATCH_SYNC);
		},

		save: function () {
			var myDirPrefId = window.arguments[0].dirPrefId;
			cardbookPreferences.setName(myDirPrefId, document.getElementById('nameTextBox').value);
			cardbookPreferences.setColor(myDirPrefId, document.getElementById('colorInput').value);
			cardbookPreferences.setVCardVersion(myDirPrefId, document.getElementById('vCardVersionTextBox').value);
			cardbookPreferences.setReadOnly(myDirPrefId, document.getElementById('readonlyCheckBox').checked);
			cardbookPreferences.setUrnuuid(myDirPrefId, document.getElementById('urnuuidCheckBox').checked);
			cardbookPreferences.setAutoSyncEnabled(myDirPrefId, document.getElementById('autoSyncCheckBox').checked);
			cardbookPreferences.setAutoSyncInterval(myDirPrefId, document.getElementById('autoSyncIntervalTextBox').value);
			cardbookPreferences.setFnFormula(myDirPrefId, document.getElementById('fnFormulaTextBox').value);
			
			if (document.getElementById('autoSyncCheckBox').checked) {
				cardbookSynchronization.removePeriodicSync(myDirPrefId, document.getElementById('nameTextBox').value);
				cardbookSynchronization.addPeriodicSync(myDirPrefId, document.getElementById('nameTextBox').value, document.getElementById('autoSyncIntervalTextBox').value);
			} else {
				cardbookSynchronization.removePeriodicSync(myDirPrefId, document.getElementById('nameTextBox').value);
			}
			
			window.arguments[0].serverCallback("SAVE", myDirPrefId, document.getElementById('nameTextBox').value,
												document.getElementById('readonlyCheckBox').checked);
			close();
		},

		cancel: function () {
			window.arguments[0].serverCallback("CANCEL", window.arguments[0].dirPrefId);
			close();
		}

	};

};
