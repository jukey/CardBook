if ("undefined" == typeof(wdw_addressbooksAdd)) {
	try {
		ChromeUtils.import("resource:///modules/mailServices.js");
		ChromeUtils.import("chrome://cardbook/content/cardbookRepository.js");
	}
	catch(e) {
		Components.utils.import("resource:///modules/mailServices.js");
		Components.utils.import("chrome://cardbook/content/cardbookRepository.js");
	}

	var wdw_addressbooksAdd = {

		gRunningDirPrefId : [],
		gFile : {},
		gCardDAVURLs : [],
		// [ [ AB type, URL, username, AB name, vCard version, AB type action, source id, collected true|false] ]
		gAccountsFound : [],
		gFinishParams : [],
		gValidateURL : false,
		gValidateDescription : "Validation module",
		gSearchDefinition : {},
		gFirstFirstStepDone : false,
		
		lTimerRefreshTokenAll : {},
		lTimerDiscoveryAll : {},
		
		initSearchDefinition: function () {
			if (cardbookRepository.cardbookComplexSearch[window.arguments[0].dirPrefId]) {
				wdw_addressbooksAdd.gSearchDefinition['searchAB'] = cardbookRepository.cardbookComplexSearch[window.arguments[0].dirPrefId].searchAB;
				wdw_addressbooksAdd.gSearchDefinition['matchAll'] = cardbookRepository.cardbookComplexSearch[window.arguments[0].dirPrefId].matchAll;
				wdw_addressbooksAdd.gSearchDefinition['rules'] = JSON.parse(JSON.stringify(cardbookRepository.cardbookComplexSearch[window.arguments[0].dirPrefId].rules));
			} else {
				wdw_addressbooksAdd.gSearchDefinition['searchAB'] = true;
				wdw_addressbooksAdd.gSearchDefinition['matchAll'] = 'and';
				wdw_addressbooksAdd.gSearchDefinition['rules'] = [["","","",""]];
			}
		},

		loadWizard: function () {
			if (window.arguments[0].action == "first") {
				wdw_addressbooksAdd.loadStandardAddressBooks();
				document.getElementById('addressbook-wizard').goTo("welcomePage");
			} else if (window.arguments[0].action == "search") {
				wdw_addressbooksAdd.initSearchDefinition();
				document.getElementById('addressbook-wizard').goTo("searchPage");
			} else if (window.arguments[0].action == "discovery") {
				wdw_addressbooksAdd.gAccountsFound = window.arguments[0].accountsToAdd;
				document.getElementById('addressbook-wizard').goTo("namesPage");
			} else {
				document.getElementById('addressbook-wizard').goTo("initialPage");
			}
		},

		loadStandardAddressBooks: function () {
			var contactManager = MailServices.ab;
			var contacts = contactManager.directories;
			while ( contacts.hasMoreElements() ) {
				var contact = contacts.getNext().QueryInterface(Components.interfaces.nsIAbDirectory);
				if (contact.dirPrefId == "ldap_2.servers.history") {
					wdw_addressbooksAdd.gAccountsFound.push(["STANDARD", "", "", contact.dirName, cardbookRepository.supportedVersion, "", contact.dirPrefId, true]);
				} else {
					wdw_addressbooksAdd.gAccountsFound.push(["STANDARD", "", "", contact.dirName, cardbookRepository.supportedVersion, "", contact.dirPrefId, false]);
				}
			}
		},

		checkRequired: function () {
			var canAdvance = true;
			var curPage = document.getElementById('addressbook-wizard').currentPage;
			if (curPage) {
				let eList = curPage.getElementsByAttribute('required', 'true');
				for (let i = 0; i < eList.length && canAdvance; ++i) {
					canAdvance = (eList[i].value != "");
				}
				document.getElementById('addressbook-wizard').canAdvance = canAdvance;
			}
		},

		checkFindLinesRequired: function (aArray) {
			var canAdvance = false;
			var i = 0;
			while (true) {
				if (document.getElementById('findPageValidateButton' + i)) {
					if (document.getElementById('findPageValidateButton' + i).getAttribute('validated') == "true") {
						canAdvance = true;
						break;
					}
					i++;
				} else {
					break;
				}
			}
			document.getElementById('addressbook-wizard').canAdvance = canAdvance;
		},

		checkNamesLinesRequired: function () {
			var canAdvance = true;
			var oneChecked = false;
			var i = 0;
			while (true) {
				if (document.getElementById('namesCheckbox' + i)) {
					var aCheckbox = document.getElementById('namesCheckbox' + i);
					var aAddressbookName = document.getElementById('namesTextbox' + i);
					if (aCheckbox.checked) {
						oneChecked = true;
						 if (aAddressbookName.value == "") {
						 	 canAdvance = false;
						 	 break;
						 }
					}
					i++;
				} else {
					break;
				}
			}
			document.getElementById('addressbook-wizard').canAdvance = (canAdvance && oneChecked);
		},

		initialPageAdvance: function () {
			var type = document.getElementById('addressbookType').selectedItem.value;
			var page = document.getElementsByAttribute('pageid', 'initialPage')[0];
			if (type == 'local') {
				page.next = 'localPage';
			} else if (type == 'remote') {
				page.next = 'remotePage';
			} else if (type == 'standard') {
				wdw_addressbooksAdd.loadStandardAddressBooks();
				page.next = 'namesPage';
			} else if (type == 'find') {
				page.next = 'findPage';
			} else if (type == 'search') {
				wdw_addressbooksAdd.initSearchDefinition();
				page.next = 'searchPage';
			}
		},

		localPageSelect: function () {
			document.getElementById('localPageURI').value = "";
			var type = document.getElementById('localPageType').selectedItem.value;
			if (type == "createDB") {
				document.getElementById('localPageURI').setAttribute('required', 'false');
				document.getElementById('localPageURILabel').setAttribute('disabled', 'true');
				document.getElementById('localPageURI').setAttribute('disabled', 'true');
				document.getElementById('localPageURIButton').setAttribute('disabled', 'true');
			} else {
				document.getElementById('localPageURI').setAttribute('required', 'true');
				document.getElementById('localPageURILabel').setAttribute('disabled', 'false');
				document.getElementById('localPageURI').setAttribute('disabled', 'false');
				document.getElementById('localPageURIButton').setAttribute('disabled', 'false');
			}
			wdw_addressbooksAdd.checkRequired();
		},

		localPageAdvance: function () {
			wdw_addressbooksAdd.gAccountsFound = [];
			var type = document.getElementById('localPageType').selectedItem.value;
			switch(type) {
				case "createDB":
					wdw_addressbooksAdd.gAccountsFound.push(["LOCALDB",
																"",
																"",
																"",
																cardbookRepository.supportedVersion,
																"",
																"",
																false]);
					break;
				case "createDirectory":
					wdw_addressbooksAdd.gAccountsFound.push(["DIRECTORY",
																"",
																"",
																wdw_addressbooksAdd.gFile.leafName,
																cardbookRepository.supportedVersion,
																"CREATEDIRECTORY",
																"",
																false]);
					break;
				case "createFile":
					wdw_addressbooksAdd.gAccountsFound.push(["FILE",
																"",
																"",
																wdw_addressbooksAdd.gFile.leafName,
																cardbookRepository.supportedVersion,
																"CREATEFILE",
																"",
																false]);
					break;
				case "openDirectory":
					wdw_addressbooksAdd.gAccountsFound.push(["DIRECTORY",
																"",
																"",
																wdw_addressbooksAdd.gFile.leafName,
																cardbookRepository.supportedVersion,
																"OPENDIRECTORY",
																"",
																false]);
					break;
				case "openFile":
					wdw_addressbooksAdd.gAccountsFound.push(["FILE",
																"",
																"",
																wdw_addressbooksAdd.gFile.leafName,
																cardbookRepository.supportedVersion,
																"OPENFILE",
																"",
																false]);
					break;
			}
		},

		searchFile: function () {
			cardbookNotifications.setNotification("localPageURINotifications", "OK");
			var type = document.getElementById('localPageType').selectedItem.value;
			switch(type) {
				case "createDirectory":
				case "openDirectory":
				case "standard":
					cardbookUtils.callDirPicker("dirChooseTitle", wdw_addressbooksAdd.checkFile);
					break;
				case "createFile":
					cardbookUtils.callFilePicker("fileCreationVCFTitle", "SAVE", "VCF", "", wdw_addressbooksAdd.checkFile);
					break;
				case "openFile":
					cardbookUtils.callFilePicker("fileSelectionVCFTitle", "OPEN", "VCF", "", wdw_addressbooksAdd.checkFile);
					break;
			}
		},

		checkFile: function (aFile) {
			var myTextbox = document.getElementById('localPageURI');
			var type = document.getElementById('localPageType').selectedItem.value;
			if (aFile != null && aFile !== undefined && aFile != "") {
				if (type == 'openFile' || type == 'createFile') {
					if (cardbookUtils.isFileAlreadyOpen(aFile.path)) {
						cardbookNotifications.setNotification("localPageURINotifications", "fileAlreadyOpen", [aFile.path]);
					} else {
						myTextbox.value = aFile.path;
						wdw_addressbooksAdd.gFile = aFile;
					}
				} else {
					if (cardbookUtils.isDirectoryAlreadyOpen(aFile.path)) {
						cardbookNotifications.setNotification("localPageURINotifications", "directoryAlreadyOpen", [aFile.path]);
					} else {
						myTextbox.value = aFile.path;
						wdw_addressbooksAdd.gFile = aFile;
					}
				}
			}
			wdw_addressbooksAdd.checkRequired();
		},

		checklocationNetwork: function () {
			var canValidate = true;
			var curPage = document.getElementById('addressbook-wizard').currentPage;
			if (curPage) {
				if (wdw_addressbooksAdd.gValidateURL) {
					document.getElementById('addressbook-wizard').canAdvance = wdw_addressbooksAdd.gValidateURL;
					document.getElementById('validateButton').disabled = !wdw_addressbooksAdd.gValidateURL;
				} else {
					document.getElementById('addressbook-wizard').canAdvance = wdw_addressbooksAdd.gValidateURL;
					let eList = curPage.getElementsByAttribute('required', 'true');
					for (let i = 0; i < eList.length && canValidate; ++i) {
						canValidate = (eList[i].value != "");
					}
					document.getElementById('validateButton').disabled = !canValidate;
				}
			}
		},

		remotePageSelect: function () {
			wdw_addressbooksAdd.gValidateURL = false;
			document.getElementById('remotePageURI').value = "";
			document.getElementById('remotePageUsername').value = "";
			document.getElementById('remotePagePassword').value = "";
			
			var type = document.getElementById('remotePageType').selectedItem.value;
			if (type == 'GOOGLE' || type == 'YAHOO') {
				document.getElementById('remotePageUriLabel').disabled=true;
				document.getElementById('remotePageURI').disabled=true;
				document.getElementById('remotePageURI').setAttribute('required', 'false');
				document.getElementById('remotePagePasswordLabel').disabled=true;
				document.getElementById('remotePagePassword').disabled=true;
				document.getElementById('remotePagePassword').setAttribute('required', 'false');
				document.getElementById('passwordCheckBox').disabled=true;
			} else if (type == 'APPLE') {
				document.getElementById('remotePageUriLabel').disabled=true;
				document.getElementById('remotePageURI').disabled=true;
				document.getElementById('remotePageURI').setAttribute('required', 'false');
				document.getElementById('remotePagePasswordLabel').disabled=false;
				document.getElementById('remotePagePassword').disabled=false;
				document.getElementById('remotePagePassword').setAttribute('required', 'true');
				document.getElementById('passwordCheckBox').disabled=false;
			} else {
				document.getElementById('remotePageUriLabel').disabled=false;
				document.getElementById('remotePageURI').disabled=false;
				document.getElementById('remotePageURI').setAttribute('required', 'true');
				document.getElementById('remotePagePasswordLabel').disabled=false;
				document.getElementById('remotePagePassword').disabled=false;
				document.getElementById('remotePagePassword').setAttribute('required', 'true');
				document.getElementById('passwordCheckBox').disabled=false;
			}
			wdw_addressbooksAdd.checklocationNetwork();
			cardbookNotifications.setNotification("resultNotifications", "OK");
		},

		remotePageTextboxInput: function () {
			wdw_addressbooksAdd.gValidateURL = false;
			wdw_addressbooksAdd.checklocationNetwork();
			cardbookNotifications.setNotification("resultNotifications", "OK");
		},

		remotePageAdvance: function () {
			let myType = document.getElementById('remotePageType').selectedItem.value;
			// APPLE or CARDDAV have already been added to gAccountsFound
			if (myType == "GOOGLE" || myType == "YAHOO") {
				wdw_addressbooksAdd.gAccountsFound = [];
				wdw_addressbooksAdd.gAccountsFound.push([myType,
															cardbookRepository.cardbookOAuthData[myType].ROOT_API,
															document.getElementById('remotePageUsername').value,
															document.getElementById('remotePageUsername').value,
															cardbookRepository.cardbookOAuthData[myType].VCARD_VERSIONS,
															"",
															"",
															false]);
			}
		},

		constructComplexSearch: function () {
			var ABList = document.getElementById('addressbookMenulist');
			var ABPopup = document.getElementById('addressbookMenupopup');
			cardbookElementTools.loadAddressBooks(ABPopup, ABList, wdw_addressbooksAdd.gSearchDefinition.searchAB, true, true, true, false, false);
			cardbookComplexSearch.loadMatchAll(wdw_addressbooksAdd.gSearchDefinition.matchAll);
			cardbookComplexSearch.constructDynamicRows("searchTerms", wdw_addressbooksAdd.gSearchDefinition.rules, "3.0");
			document.getElementById('searchTerms_0_valueBox').focus();
		},

		checkSearch: function () {
			wdw_addressbooksAdd.constructComplexSearch();
			document.getElementById('addressbook-wizard').canAdvance = false;
			function checkTerms() {
				if (cardbookComplexSearch.getSearch() != "") {
					document.getElementById('addressbook-wizard').canAdvance = true;
				} else {
					document.getElementById('addressbook-wizard').canAdvance = false;
				}
			};
			checkTerms();
			document.getElementById('searchTerms').addEventListener("input", checkTerms, false);
			document.getElementById('searchTerms').addEventListener("command", checkTerms, false);
			document.getElementById('searchTerms').addEventListener("click", checkTerms, false);
		},

		searchPageAdvance: function () {
			let mySearch = cardbookComplexSearch.getSearch();

			var relative = mySearch.match("^searchAB:([^:]*):searchAll:([^:]*)(.*)");
			wdw_addressbooksAdd.gSearchDefinition.searchAB = relative[1];
			if (relative[2] == "true") {
				wdw_addressbooksAdd.gSearchDefinition.matchAll = true;
			} else {
				wdw_addressbooksAdd.gSearchDefinition.matchAll = false;
			}
			var tmpRuleArray = relative[3].split(/:case:/);
			wdw_addressbooksAdd.gSearchDefinition.rules = [];
			for (var i = 1; i < tmpRuleArray.length; i++) {
				var relative = tmpRuleArray[i].match("([^:]*):field:([^:]*):term:([^:]*):value:([^:]*)");
				wdw_addressbooksAdd.gSearchDefinition.rules.push([relative[1], relative[2], relative[3], relative[4]]);
			}
		},

		showPassword: function () {
			var passwordType = document.getElementById('remotePagePassword').type;
			if (passwordType != "password") {
				document.getElementById('remotePagePassword').type = "password";
			} else {
				document.getElementById('remotePagePassword').type = "";
			}
		},

		validateURL: function () {
			document.getElementById('addressbook-wizard').canAdvance = false;
			document.getElementById('remotePageURI').value = cardbookUtils.decodeURL(document.getElementById('remotePageURI').value.trim());
			document.getElementById('validateButton').disabled = true;

			var type = document.getElementById('remotePageType').selectedItem.value;
			var username = document.getElementById('remotePageUsername').value;
			var password = document.getElementById('remotePagePassword').value;
			if (type == 'GOOGLE') {
				var url = cardbookRepository.cardbookOAuthData.GOOGLE.ROOT_API;
			} else if (type == 'YAHOO') {
				var url = cardbookRepository.cardbookOAuthData.YAHOO.ROOT_API;
			} else if (type == 'APPLE') {
				var url = cardbookRepository.APPLE_API;
				wdw_addressbooksAdd.gCardDAVURLs.push([cardbookSynchronization.getSlashedUrl(url), true]); // [url, discovery]
			} else {
				var url = document.getElementById('remotePageURI').value;
				if (cardbookSynchronization.getRootUrl(url) == "") {
					cardbookNotifications.setNotification("resultNotifications", "ValidatingURLFailedLabel");
					return;
				}
				wdw_addressbooksAdd.gCardDAVURLs.push([url, false]); // [url, discovery]
				wdw_addressbooksAdd.gCardDAVURLs.push([cardbookSynchronization.getWellKnownUrl(url), true]);
				var carddavURL = cardbookSynchronization.getCardDAVUrl(url, username);
				if (carddavURL != "") {
					wdw_addressbooksAdd.gCardDAVURLs.push([carddavURL, false]);
				}
			}
			
			var dirPrefId = cardbookUtils.getUUID();
			if (type == 'GOOGLE') {
				cardbookNotifications.setNotification("resultNotifications", "Validating1Label", [url], "PRIORITY_INFO_MEDIUM");
				cardbookSynchronization.initMultipleOperations(dirPrefId);
				cardbookRepository.cardbookServerSyncRequest[dirPrefId]++;
				var connection = {connUser: username, connPrefId: dirPrefId, connDescription: wdw_addressbooksAdd.gValidateDescription};
				cardbookSynchronizationGoogle.requestNewRefreshTokenForGoogle(connection, null, type, null);
				wdw_addressbooksAdd.waitForRefreshTokenFinished(dirPrefId, url);
			} else if (type == 'YAHOO') {
				cardbookNotifications.setNotification("resultNotifications", "Validating1Label", [url], "PRIORITY_INFO_MEDIUM");
				cardbookSynchronization.initMultipleOperations(dirPrefId);
				cardbookRepository.cardbookServerSyncRequest[dirPrefId]++;
				var connection = {connUser: username, connPrefId: dirPrefId, connDescription: wdw_addressbooksAdd.gValidateDescription};
				cardbookSynchronizationYahoo.requestNewRefreshTokenForYahoo(connection, null, type, null);
				wdw_addressbooksAdd.waitForRefreshTokenFinished(dirPrefId, url);
			} else {
				cardbookSynchronization.initDiscovery(dirPrefId);
				wdw_addressbooksAdd.validateCardDAVURL(dirPrefId, username, password, type);
			}
		},

		validateCardDAVURL: function (aDirPrefId, aUsername, aPassword, aType) {
			cardbookPreferences.setId(aDirPrefId, aDirPrefId);
			cardbookPreferences.setUrl(aDirPrefId, wdw_addressbooksAdd.gCardDAVURLs[0][0]);
			wdw_addressbooksAdd.gRunningDirPrefId.push(aDirPrefId);
			cardbookPasswordManager.removeAccount(aUsername, wdw_addressbooksAdd.gCardDAVURLs[0][0]);
			cardbookPasswordManager.addAccount(aUsername, wdw_addressbooksAdd.gCardDAVURLs[0][0], aPassword);
			
			if (wdw_addressbooksAdd.gCardDAVURLs.length > 0) {
				cardbookNotifications.setNotification("resultNotifications", "Validating1Label", [wdw_addressbooksAdd.gCardDAVURLs[0][0]], "PRIORITY_INFO_MEDIUM");
				cardbookSynchronization.initMultipleOperations(aDirPrefId);
				cardbookRepository.cardbookServerValidation[aDirPrefId] = {length: 0, user: aUsername};
				cardbookRepository.cardbookServerSyncRequest[aDirPrefId]++;
				var connection = {connUser: aUsername, connPrefId: aDirPrefId, connUrl: wdw_addressbooksAdd.gCardDAVURLs[0][0], connDescription: wdw_addressbooksAdd.gValidateDescription};
				var params = {aPrefIdType: aType};
				if (wdw_addressbooksAdd.gCardDAVURLs[0][1]) {
					cardbookSynchronization.discoverPhase1(connection, "GETDISPLAYNAME", params);
				} else {
					cardbookSynchronization.validateWithoutDiscovery(connection, "GETDISPLAYNAME", params);
				}
				wdw_addressbooksAdd.waitForDiscoveryFinished(aDirPrefId, aUsername, aPassword, aType);
			}
		},

		validateFindLine: function (aRowId) {
			if (document.getElementById('findPageValidateButton' + aRowId).getAttribute('validated') == "true") {
				return;
			}
			var dirPrefId = cardbookUtils.getUUID();
			var myType = document.getElementById('findPageValidateButton' + aRowId).getAttribute('validationType');
			var myURL = document.getElementById('findPageURLTextbox' + aRowId).value;
			var myUsername = document.getElementById('findUsernameTextbox' + aRowId).value;

			if (myType == 'GOOGLE') {
				cardbookSynchronization.initMultipleOperations(dirPrefId);
				cardbookRepository.cardbookServerSyncRequest[dirPrefId]++;
				var connection = {connUser: myUsername, connPrefId: dirPrefId, connDescription: wdw_addressbooksAdd.gValidateDescription};
				cardbookSynchronizationGoogle.requestNewRefreshTokenForGoogle(connection, null, myType, null);
				wdw_addressbooksAdd.waitForFindRefreshTokenFinished(aRowId, dirPrefId, myURL);
			} else if (myType == 'YAHOO') {
				cardbookSynchronization.initMultipleOperations(dirPrefId);
				cardbookRepository.cardbookServerSyncRequest[dirPrefId]++;
				var connection = {connUser: myUsername, connPrefId: dirPrefId, connDescription: wdw_addressbooksAdd.gValidateDescription};
				cardbookSynchronizationYahoo.requestNewRefreshTokenForYahoo(connection, null, myType, null);
				wdw_addressbooksAdd.waitForFindRefreshTokenFinished(aRowId, dirPrefId, myURL);
			} else {
				var myPassword = document.getElementById('findPasswordTextbox' + aRowId).value;
				cardbookSynchronization.initDiscovery(dirPrefId);
				cardbookPreferences.setId(dirPrefId, dirPrefId);
				cardbookPreferences.setUrl(dirPrefId, myURL);
				wdw_addressbooksAdd.gRunningDirPrefId.push(dirPrefId);
				cardbookPasswordManager.removeAccount(aUsername, myURL);
				cardbookPasswordManager.addAccount(aUsername, myURL, myPassword);
				
				cardbookSynchronization.initMultipleOperations(dirPrefId);
				cardbookRepository.cardbookServerValidation[dirPrefId] = {length: 0, user: myUsername};
				cardbookRepository.cardbookServerSyncRequest[dirPrefId]++;
				var connection = {connUser: myUsername, connPrefId: dirPrefId, connUrl: myURL, connDescription: wdw_addressbooksAdd.gValidateDescription};
				var params = {aPrefIdType: myType};
				cardbookSynchronization.validateWithoutDiscovery(connection, "GETDISPLAYNAME", params);
				wdw_addressbooksAdd.waitForFindDiscoveryFinished(aRowId, dirPrefId, myUsername, myPassword, myType);
			}
		},

		waitForDiscoveryFinished: function (aDirPrefId, aUsername, aPassword, aType) {
			wdw_addressbooksAdd.lTimerDiscoveryAll[aDirPrefId] = Components.classes["@mozilla.org/timer;1"].createInstance(Components.interfaces.nsITimer);
			var lTimerDiscovery = wdw_addressbooksAdd.lTimerDiscoveryAll[aDirPrefId];
			lTimerDiscovery.initWithCallback({ notify: function(lTimerDiscovery) {
						wdw_cardbooklog.updateStatusProgressInformationWithDebug1(wdw_addressbooksAdd.gValidateDescription + " : debug mode : cardbookRepository.cardbookServerDiscoveryRequest : ", cardbookRepository.cardbookServerDiscoveryRequest[aDirPrefId]);
						wdw_cardbooklog.updateStatusProgressInformationWithDebug1(wdw_addressbooksAdd.gValidateDescription + " : debug mode : cardbookRepository.cardbookServerDiscoveryResponse : ", cardbookRepository.cardbookServerDiscoveryResponse[aDirPrefId]);
						wdw_cardbooklog.updateStatusProgressInformationWithDebug1(wdw_addressbooksAdd.gValidateDescription + " : debug mode : cardbookRepository.cardbookServerDiscoveryError : ", cardbookRepository.cardbookServerDiscoveryError[aDirPrefId]);
						wdw_cardbooklog.updateStatusProgressInformationWithDebug1(wdw_addressbooksAdd.gValidateDescription + " : debug mode : cardbookRepository.cardbookServerValidation : ", cardbookRepository.cardbookServerValidation[aDirPrefId]);
						if (cardbookRepository.cardbookServerDiscoveryError[aDirPrefId] >= 1) {
							wdw_addressbooksAdd.gCardDAVURLs.shift();
							if (cardbookRepository.cardbookServerValidation[aDirPrefId] && cardbookRepository.cardbookServerValidation[aDirPrefId].length == 0) {
								cardbookSynchronization.finishMultipleOperations(aDirPrefId);
								if (wdw_addressbooksAdd.gCardDAVURLs.length == 0) {
									cardbookNotifications.setNotification("resultNotifications", "ValidationFailedLabel");
									wdw_addressbooksAdd.gValidateURL = false;
									wdw_addressbooksAdd.checklocationNetwork();
									lTimerDiscovery.cancel();
								} else {
									document.getElementById('validateButton').disabled = true;
									lTimerDiscovery.cancel();
									wdw_addressbooksAdd.validateCardDAVURL(aDirPrefId, aUsername, aPassword, aType);
								}
							} else {
								cardbookSynchronization.finishMultipleOperations(aDirPrefId);
								cardbookNotifications.setNotification("resultNotifications", "ValidationFailedLabel");
								wdw_addressbooksAdd.gValidateURL = false;
								wdw_addressbooksAdd.checklocationNetwork();
								lTimerDiscovery.cancel();
							}
						} else if (cardbookRepository.cardbookServerDiscoveryRequest[aDirPrefId] !== cardbookRepository.cardbookServerDiscoveryResponse[aDirPrefId] || cardbookRepository.cardbookServerDiscoveryResponse[aDirPrefId] === 0) {
							cardbookNotifications.setNotification("resultNotifications", "Validating1Label", [wdw_addressbooksAdd.gCardDAVURLs[0][0]], "PRIORITY_INFO_MEDIUM");
						} else {
							wdw_addressbooksAdd.gCardDAVURLs.shift();
							if (cardbookRepository.cardbookServerValidation[aDirPrefId] && cardbookRepository.cardbookServerValidation[aDirPrefId].length == 0) {
								cardbookSynchronization.finishMultipleOperations(aDirPrefId);
								if (wdw_addressbooksAdd.gCardDAVURLs.length == 0) {
									cardbookNotifications.setNotification("resultNotifications", "ValidationFailedLabel");
									wdw_addressbooksAdd.gValidateURL = false;
									wdw_addressbooksAdd.checklocationNetwork();
									lTimerDiscovery.cancel();
								} else {
									document.getElementById('validateButton').disabled = true;
									lTimerDiscovery.cancel();
									wdw_addressbooksAdd.validateCardDAVURL(aDirPrefId, aUsername, aPassword, aType);
								}
							} else {
								wdw_addressbooksAdd.gCardDAVURLs = [];
								cardbookNotifications.setNotification("resultNotifications", "OK");
								wdw_addressbooksAdd.gValidateURL = true;
								wdw_addressbooksAdd.checklocationNetwork();
								wdw_addressbooksAdd.gAccountsFound = cardbookUtils.fromValidationToArray(aDirPrefId, aType);
								cardbookSynchronization.stopDiscovery(aDirPrefId);
								cardbookSynchronization.finishMultipleOperations(aDirPrefId);
								lTimerDiscovery.cancel();
							}
						}
					}
					}, 1000, Components.interfaces.nsITimer.TYPE_REPEATING_SLACK);
		},

		waitForFindDiscoveryFinished: function (aRowId, aDirPrefId, aUsername, aPassword, aType) {
			wdw_addressbooksAdd.lTimerDiscoveryAll[aDirPrefId] = Components.classes["@mozilla.org/timer;1"].createInstance(Components.interfaces.nsITimer);
			var lTimerDiscovery = wdw_addressbooksAdd.lTimerDiscoveryAll[aDirPrefId];
			lTimerDiscovery.initWithCallback({ notify: function(lTimerDiscovery) {
						wdw_cardbooklog.updateStatusProgressInformationWithDebug1(wdw_addressbooksAdd.gValidateDescription + " : debug mode : cardbookRepository.cardbookServerDiscoveryRequest : ", cardbookRepository.cardbookServerDiscoveryRequest[aDirPrefId]);
						wdw_cardbooklog.updateStatusProgressInformationWithDebug1(wdw_addressbooksAdd.gValidateDescription + " : debug mode : cardbookRepository.cardbookServerDiscoveryResponse : ", cardbookRepository.cardbookServerDiscoveryResponse[aDirPrefId]);
						wdw_cardbooklog.updateStatusProgressInformationWithDebug1(wdw_addressbooksAdd.gValidateDescription + " : debug mode : cardbookRepository.cardbookServerDiscoveryError : ", cardbookRepository.cardbookServerDiscoveryError[aDirPrefId]);
						wdw_cardbooklog.updateStatusProgressInformationWithDebug1(wdw_addressbooksAdd.gValidateDescription + " : debug mode : cardbookRepository.cardbookServerValidation : ", cardbookRepository.cardbookServerValidation[aDirPrefId]);
						var myButton = document.getElementById('findPageValidateButton' + aRowId);
						if (cardbookRepository.cardbookServerDiscoveryError[aDirPrefId] >= 1) {
							myButton.setAttribute('validated', 'false');
							myButton.setAttribute('label', cardbookRepository.strBundle.GetStringFromName("ValidationFailedLabel"));
							wdw_addressbooksAdd.checkFindLinesRequired();
							cardbookSynchronization.finishMultipleOperations(aDirPrefId);
							lTimerDiscovery.cancel();
						} else if (cardbookRepository.cardbookServerDiscoveryRequest[aDirPrefId] !== cardbookRepository.cardbookServerDiscoveryResponse[aDirPrefId] || cardbookRepository.cardbookServerDiscoveryResponse[aDirPrefId] === 0) {
							myButton.setAttribute('label', cardbookRepository.strBundle.GetStringFromName("Validating2Label"));
						} else {
							myButton.setAttribute('validated', 'true');
							myButton.setAttribute('label', cardbookRepository.strBundle.GetStringFromName("ValidationOKLabel"));
							wdw_addressbooksAdd.checkFindLinesRequired();
							cardbookSynchronization.finishMultipleOperations(aDirPrefId);
							lTimerRefreshToken.cancel();
						}
					}
					}, 1000, Components.interfaces.nsITimer.TYPE_REPEATING_SLACK);
		},

		waitForRefreshTokenFinished: function (aPrefId, aUrl) {
			wdw_addressbooksAdd.lTimerRefreshTokenAll[aPrefId] = Components.classes["@mozilla.org/timer;1"].createInstance(Components.interfaces.nsITimer);
			var lTimerRefreshToken = wdw_addressbooksAdd.lTimerRefreshTokenAll[aPrefId];
			lTimerRefreshToken.initWithCallback({ notify: function(lTimerRefreshToken) {
						if (cardbookRepository.cardbookRefreshTokenError[aPrefId] >= 1) {
							cardbookNotifications.setNotification("resultNotifications", "ValidationFailedLabel");
							wdw_addressbooksAdd.gValidateURL = false;
							wdw_addressbooksAdd.checklocationNetwork();
							cardbookSynchronization.finishMultipleOperations(aPrefId);
							lTimerRefreshToken.cancel();
						} else if (cardbookRepository.cardbookRefreshTokenResponse[aPrefId] !== 1) {
							cardbookNotifications.setNotification("resultNotifications", "Validating1Label", [aUrl], "PRIORITY_INFO_MEDIUM");
						} else {
							cardbookNotifications.setNotification("resultNotifications", "OK");
							wdw_addressbooksAdd.gValidateURL = true;
							wdw_addressbooksAdd.checklocationNetwork();
							cardbookSynchronization.finishMultipleOperations(aPrefId);
							lTimerRefreshToken.cancel();
						}
					}
					}, 1000, Components.interfaces.nsITimer.TYPE_REPEATING_SLACK);
		},

		waitForFindRefreshTokenFinished: function (aRowId, aPrefId, aUrl) {
			wdw_addressbooksAdd.lTimerRefreshTokenAll[aPrefId] = Components.classes["@mozilla.org/timer;1"].createInstance(Components.interfaces.nsITimer);
			var lTimerRefreshToken = wdw_addressbooksAdd.lTimerRefreshTokenAll[aPrefId];
			lTimerRefreshToken.initWithCallback({ notify: function(lTimerRefreshToken) {
						var myButton = document.getElementById('findPageValidateButton' + aRowId);
						if (cardbookRepository.cardbookRefreshTokenError[aPrefId] >= 1) {
							myButton.setAttribute('validated', 'false');
							myButton.setAttribute('label', cardbookRepository.strBundle.GetStringFromName("ValidationFailedLabel"));
							wdw_addressbooksAdd.checkFindLinesRequired();
							cardbookSynchronization.finishMultipleOperations(aPrefId);
							lTimerRefreshToken.cancel();
						} else if (cardbookRepository.cardbookRefreshTokenResponse[aPrefId] !== 1) {
							myButton.setAttribute('label', cardbookRepository.strBundle.GetStringFromName("Validating2Label"));
						} else {
							myButton.setAttribute('validated', 'true');
							myButton.setAttribute('label', cardbookRepository.strBundle.GetStringFromName("ValidationOKLabel"));
							wdw_addressbooksAdd.checkFindLinesRequired();
							cardbookSynchronization.finishMultipleOperations(aPrefId);
							lTimerRefreshToken.cancel();
						}
					}
					}, 1000, Components.interfaces.nsITimer.TYPE_REPEATING_SLACK);
		},

		onSuccessfulAuthentication: function (aResponse) {
			var username = document.getElementById('remotePageUsername').value;
			cardbookPasswordManager.removeAccount(username);
			cardbookPasswordManager.addAccount(username, "", aResponse.refresh_token);
			var wizard = document.getElementById("addressbook-wizard");
			wizard.canAdvance = true;
			wizard.advance();
		},

		loadSearchName: function () {
			if (window.arguments[0].dirPrefId != null && window.arguments[0].dirPrefId !== undefined && window.arguments[0].dirPrefId != "") {
				document.getElementById('searchNamePageName').value = cardbookPreferences.getName(window.arguments[0].dirPrefId);
			}
			wdw_addressbooksAdd.checkRequired();
		},

		deleteBoxes: function (aRowName, aHeaderRowName) {
			var aListRows = document.getElementById(aRowName);
			var childNodes = aListRows.childNodes;
			var toDelete = [];
			for (var i = 0; i < childNodes.length; i++) {
				var child = childNodes[i];
				if (child.getAttribute('id') != aHeaderRowName) {
					toDelete.push(child);
				}
			}
			for (var i = 0; i < toDelete.length; i++) {
				var oldChild = aListRows.removeChild(toDelete[i]);
			}
		},

		createBoxesForNames: function (aType, aURL, aName, aVersionList, aUsername, aActionType, aSourceDirPrefId, aSourceCollected) {
			var aListRows = document.getElementById('namesRows');
			var aId = aListRows.childNodes.length - 1;
			var aRow = document.createElement('row');
			aListRows.appendChild(aRow);
			aRow.setAttribute('id', 'namesRow' + aId);
			aRow.setAttribute('flex', '1');

			var aCheckbox = document.createElement('checkbox');
			aRow.appendChild(aCheckbox);
			aCheckbox.setAttribute('checked', true);
			aCheckbox.setAttribute('id', 'namesCheckbox' + aId);
			aCheckbox.setAttribute('validationType', aType);
			aCheckbox.setAttribute('username', aUsername);
			aCheckbox.setAttribute('actionType', aActionType);
			aCheckbox.setAttribute('sourceDirPrefId', aSourceDirPrefId);
			aCheckbox.setAttribute('sourceCollected', aSourceCollected.toString());
			aCheckbox.setAttribute("aria-labelledby", "namesPageSelectedLabel");
			aCheckbox.addEventListener("command", function() {
					var aTextBox = document.getElementById('namesTextbox' + this.id.replace("namesCheckbox",""));
					if (this.checked) {
						aTextBox.setAttribute('required', true);
					} else {
						aTextBox.setAttribute('required', false);
					}
					wdw_addressbooksAdd.checkNamesLinesRequired();
				}, false);

			var aTextbox = document.createElement('textbox');
			aRow.appendChild(aTextbox);
			aTextbox.setAttribute('id', 'namesTextbox' + aId);
			aTextbox.setAttribute("aria-labelledby", "namesPageNameLabel");
			aTextbox.setAttribute('flex', '1');
			aTextbox.setAttribute('required', true);
			aTextbox.value = aName;
			aTextbox.addEventListener("input", function() {
					wdw_addressbooksAdd.checkNamesLinesRequired();
				}, false);

			var aColorbox =  document.createElementNS("http://www.w3.org/1999/xhtml","input");
			aRow.appendChild(aColorbox);
			aColorbox.setAttribute('id', 'serverColorInput' + aId);
			aColorbox.setAttribute("aria-labelledby", "namesPageColorLabel");
			aColorbox.setAttribute('class', "small-margin");
			aColorbox.setAttribute('type', "color");
			aColorbox.value = cardbookUtils.randomColor(100);
			
			var aMenuList = document.createElement('menulist');
			aRow.appendChild(aMenuList);
			aMenuList.setAttribute('id', 'vCardVersionPageName' + aId);
			aMenuList.setAttribute("aria-labelledby", "namesPageVCardVersionLabel");
			var aMenuPopup = document.createElement('menupopup');
			aMenuList.appendChild(aMenuPopup);
			aMenuPopup.setAttribute('id', 'vCardVersionPageNameMenupopup' + aId);
			cardbookElementTools.loadVCardVersions(aMenuPopup.id, aMenuList.id, aVersionList);

			var aTextbox = document.createElement('textbox');
			aRow.appendChild(aTextbox);
			aTextbox.setAttribute('id', 'URLTextbox' + aId);
			aTextbox.setAttribute("aria-labelledby", "namesPageURLLabel");
			aTextbox.setAttribute('hidden', 'true');
			aTextbox.value = aURL;

			var aCheckbox1 = document.createElement('checkbox');
			aRow.appendChild(aCheckbox1);
			aCheckbox1.setAttribute('checked', false);
			aCheckbox1.setAttribute('id', 'readonlyCheckbox' + aId);
			aCheckbox1.setAttribute("aria-labelledby", "namesPageReadonlyLabel");
			var aCheckbox2 = document.createElement('checkbox');
			aRow.appendChild(aCheckbox2);                                              
			aCheckbox2.setAttribute('checked', false);
			aCheckbox2.setAttribute('id', 'urnuuidCheckbox' + aId);
			aCheckbox2.setAttribute("aria-labelledby", "namesPageUrnuuidLabel");
		},

		loadNames: function () {
			wdw_addressbooksAdd.deleteBoxes('namesRows', 'namesHeadersRow');
			if (window.arguments[0].action == "discovery") {
				wdw_addressbooksAdd.setCanRewindFalse();
			}
			for (var i = 0; i < wdw_addressbooksAdd.gAccountsFound.length; i++) {
				if (wdw_addressbooksAdd.gAccountsFound[i][4].length > 0) {
					wdw_addressbooksAdd.createBoxesForNames(wdw_addressbooksAdd.gAccountsFound[i][0], wdw_addressbooksAdd.gAccountsFound[i][1], wdw_addressbooksAdd.gAccountsFound[i][3],
													wdw_addressbooksAdd.gAccountsFound[i][4], wdw_addressbooksAdd.gAccountsFound[i][2], wdw_addressbooksAdd.gAccountsFound[i][5], wdw_addressbooksAdd.gAccountsFound[i][6], wdw_addressbooksAdd.gAccountsFound[i][7]);
				} else {
					wdw_addressbooksAdd.createBoxesForNames(wdw_addressbooksAdd.gAccountsFound[i][0], wdw_addressbooksAdd.gAccountsFound[i][1], wdw_addressbooksAdd.gAccountsFound[i][3],
													[ "3.0" ], wdw_addressbooksAdd.gAccountsFound[i][2], wdw_addressbooksAdd.gAccountsFound[i][5], wdw_addressbooksAdd.gAccountsFound[i][6], wdw_addressbooksAdd.gAccountsFound[i][7]);
				}
			}
			wdw_addressbooksAdd.checkNamesLinesRequired();
		},

		namesAdvance: function () {
			var page = document.getElementsByAttribute('pageid', 'namesPage')[0];
			wdw_addressbooksAdd.prepareAddressbook();
			if (window.arguments[0].action == "first" && !wdw_addressbooksAdd.gFirstFirstStepDone) {
				page.next = 'finishFirstPage';
			} else {
				page.next = 'finishPage';
			}
		},

		createBoxesForFinds: function (aType, aUsername, aAddPassword, aVCardVersion, aUrl, aABName) {
			var aListRows = document.getElementById('findRows');
			var aId = aListRows.childNodes.length - 1;
			var aRow = document.createElement('row');
			aListRows.appendChild(aRow);
			aRow.setAttribute('id', 'findRows' + aId);
			aRow.setAttribute('flex', '1');
			
			var aButton = document.createElement('button');
			aRow.appendChild(aButton);
			aButton.setAttribute('id', 'findPageValidateButton' + aId);
			aButton.setAttribute("aria-labelledby", "findPageValidateLabel");
			aButton.setAttribute('flex', '1');
			aButton.setAttribute('validationType', aType);
			aButton.setAttribute('validated', 'false');
			aButton.setAttribute('label', cardbookRepository.strBundle.GetStringFromName("noValidatedEntryTooltip"));
			aButton.addEventListener("command", function() {
					var myId = this.id.replace("findPageValidateButton","");
					wdw_addressbooksAdd.validateFindLine(myId);
				}, false);

			var aTextbox = document.createElement('textbox');
			aRow.appendChild(aTextbox);
			aTextbox.setAttribute('id', 'findUsernameTextbox' + aId);
			aTextbox.setAttribute("aria-labelledby", "findPageUserLabel");
			aTextbox.setAttribute('flex', '1');
			aTextbox.setAttribute('required', true);
			aTextbox.setAttribute('disabled', true);
			aTextbox.value = aUsername;

			if (aAddPassword) {
				var aTextbox = document.createElement('textbox');
				aRow.appendChild(aTextbox);
				aTextbox.setAttribute('id', 'findPasswordTextbox' + aId);
				aTextbox.setAttribute("aria-labelledby", "findPagePasswordLabel");
				aTextbox.setAttribute('flex', '1');
				aTextbox.setAttribute('required', true);
			} else {
				var aHbox = document.createElement('hbox');
				aRow.appendChild(aHbox);
				aHbox.setAttribute('align', 'center');
				aHbox.setAttribute('flex', '1');
			}

			var aTextbox = document.createElement('textbox');
			aRow.appendChild(aTextbox);
			aTextbox.setAttribute('id', 'findPageVCardVersionsTextbox' + aId);
			aTextbox.setAttribute("aria-labelledby", "findPageVCardVersionsLabel");
			aTextbox.setAttribute('hidden', 'true');
			aTextbox.value = aVCardVersion;

			var aTextbox = document.createElement('textbox');
			aRow.appendChild(aTextbox);
			aTextbox.setAttribute('id', 'findPageURLTextbox' + aId);
			aTextbox.setAttribute("aria-labelledby", "findPageURLLabel");
			aTextbox.setAttribute('hidden', 'true');
			aTextbox.value = aUrl;
			
			var aTextbox = document.createElement('textbox');
			aRow.appendChild(aTextbox);
			aTextbox.setAttribute('id', 'findPageABNameTextbox' + aId);
			aTextbox.setAttribute("aria-labelledby", "findPageABNameLabel");
			aTextbox.setAttribute('hidden', 'true');
			aTextbox.value = aABName;
		},

		loadFinds: function () {
			wdw_addressbooksAdd.deleteBoxes('findRows', 'findHeadersRow');
			if (window.arguments[0].action == "first") {
				wdw_addressbooksAdd.setCanRewindFalse();
			}
			var sortedEmailAccounts = [];
			var accounts = MailServices.accounts.accounts;
			var accountsLength = (typeof accounts.Count === 'undefined') ? accounts.length : accounts.Count();
			for (var i = 0; i < accountsLength; i++) {
				var account = accounts.queryElementAt ? accounts.queryElementAt(i, Components.interfaces.nsIMsgAccount) : accounts.GetElementAt(i).QueryInterface(Components.interfaces.nsIMsgAccount);
				if (!account.incomingServer) {
					continue;
				}
				var identitiesLength = (typeof account.identities.Count === 'undefined') ? account.identities.length : account.identities.Count();
				for (var k = 0; k < identitiesLength; k++) {
					var identity = account.identities.queryElementAt ? account.identities.queryElementAt(k, Components.interfaces.nsIMsgIdentity) : account.identities.GetElementAt(k).QueryInterface(Components.interfaces.nsIMsgIdentity);
					var mailAccountServer = account.incomingServer;
					if (mailAccountServer.type == "pop3" || mailAccountServer.type == "imap") {
						sortedEmailAccounts.push(identity.email.toLowerCase());
					}
				}
			}
			sortedEmailAccounts = cardbookUtils.sortArrayByString(sortedEmailAccounts,1);
			sortedEmailAccounts = cardbookRepository.arrayUnique(sortedEmailAccounts);
			
			for (var i = 0; i < sortedEmailAccounts.length; i++) {
				let found = false;
				// first OAuth 
				for (var j in cardbookRepository.cardbookOAuthData) {
					if (sortedEmailAccounts[i].endsWith(cardbookRepository.cardbookOAuthData[j].EMAIL_TYPE)) {
						wdw_addressbooksAdd.createBoxesForFinds(j, sortedEmailAccounts[i], false, cardbookRepository.cardbookOAuthData[j].VCARD_VERSIONS.toString(),
																	cardbookRepository.cardbookOAuthData[j].ROOT_API, sortedEmailAccounts[i]);
						found = true;
						break;
					}
				}
				// then CARDDAV
				if (!found) {
					var test;
				}
			}
			
			if (document.getElementById('findRows').childNodes.length == 1) {
				document.getElementById('findHeadersRow').setAttribute('hidden', 'true');
				document.getElementById('findPageName1Description').removeAttribute('hidden');
				document.getElementById('findPageName2Description').setAttribute('hidden', 'true');
				document.getElementById('findPageName3Description').setAttribute('hidden', 'true');
			} else if (document.getElementById('findRows').childNodes.length == 2) {
				document.getElementById('findHeadersRow').removeAttribute('hidden');
				document.getElementById('findPageName1Description').setAttribute('hidden', 'true');
				document.getElementById('findPageName2Description').removeAttribute('hidden');
				document.getElementById('findPageName3Description').setAttribute('hidden', 'true');
			} else {
				document.getElementById('findHeadersRow').removeAttribute('hidden');
				document.getElementById('findPageName1Description').setAttribute('hidden', 'true');
				document.getElementById('findPageName2Description').setAttribute('hidden', 'true');
				document.getElementById('findPageName3Description').removeAttribute('hidden');
			}
			wdw_addressbooksAdd.checkFindLinesRequired();
		},

		findAdvance: function () {
			wdw_addressbooksAdd.gAccountsFound = [];
			var i = 0;
			while (true) {
				if (document.getElementById('findPageValidateButton' + i)) {
					if (document.getElementById('findPageValidateButton' + i).getAttribute('validated') == "true") {
						wdw_addressbooksAdd.gAccountsFound.push([document.getElementById('findPageValidateButton' + i).getAttribute('validationType'),
																	document.getElementById('findPageURLTextbox' + i).value,
																	document.getElementById('findUsernameTextbox' + i).value,
																	document.getElementById('findPageABNameTextbox' + i).value,
																	document.getElementById('findPageVCardVersionsTextbox' + i).value.split(","),
																	"",
																	"",
																	false]);
					}
					i++
				} else {
					break;
				}
			}
		},

		finishFirstPageShow: function () {
			wdw_addressbooksAdd.prepareSearchAllContactsAddressbook();
			wdw_addressbooksAdd.createAddressbook();
			wdw_addressbooksAdd.gFirstFirstStepDone = true;
			wdw_addressbooksAdd.setCanRewindFalse();
			if (wdw_addressbooksAdd.gFinishParams.length > 1) {
				document.getElementById('finishFirstPage1Description').setAttribute('hidden', 'true');
				document.getElementById('finishFirstPage2Description').removeAttribute('hidden');
			} else {
				document.getElementById('finishFirstPage1Description').removeAttribute('hidden');
				document.getElementById('finishFirstPage2Description').setAttribute('hidden', 'true');
			}
		},

		finishPageShow: function () {
			wdw_addressbooksAdd.setCanRewindFalse();
			if (wdw_addressbooksAdd.gFinishParams.length > 1) {
				document.getElementById('finishPage1Description').setAttribute('hidden', 'true');
				document.getElementById('finishPage2Description').removeAttribute('hidden');
			} else {
				document.getElementById('finishPage1Description').removeAttribute('hidden');
				document.getElementById('finishPage2Description').setAttribute('hidden', 'true');
			}
		},

		prepareSearchAllContactsAddressbook: function () {
			var dirPrefId = cardbookUtils.getUUID();
			var myName = cardbookRepository.strBundle.GetStringFromName("allContacts");
			wdw_addressbooksAdd.gFinishParams.push({type: "SEARCH", searchDef:"searchAB:allAddressBooks:searchAll:true:case:dig:field:version:term:IsntEmpty:value:",
														name: myName, username: "", color: "", vcard: "", enabled: true,
														dirPrefId: dirPrefId, DBcached: false, firstAction: false});
		},

		prepareSearchAddressbook: function () {
			var name = document.getElementById('searchNamePageName').value;
			if (window.arguments[0].dirPrefId != null && window.arguments[0].dirPrefId !== undefined && window.arguments[0].dirPrefId != "") {
				var dirPrefId = window.arguments[0].dirPrefId;
				var enabled = cardbookPreferences.getEnabled(window.arguments[0].dirPrefId);
			} else {
				var dirPrefId = cardbookUtils.getUUID();
				var enabled = true;
			}
			wdw_addressbooksAdd.gFinishParams.push({type: "SEARCH", searchDef: cardbookComplexSearch.getSearch(), name: name, username: "", color: "", vcard: "", enabled: enabled,
														dirPrefId: dirPrefId, DBcached: false, firstAction: false});
		},

		prepareAddressbook: function () {
			wdw_addressbooksAdd.gFinishParams = [];
			var i = 0;
			while (true) {
				if (document.getElementById('namesCheckbox' + i)) {
					var aCheckbox = document.getElementById('namesCheckbox' + i);
					if (aCheckbox.checked) {
						var myType = aCheckbox.getAttribute('validationType');
						var aAddressbookId = cardbookUtils.getUUID();
						var aAddressbookName = document.getElementById('namesTextbox' + i).value;
						var aAddressbookColor = document.getElementById('serverColorInput' + i).value;
						var aAddressbookVCard = document.getElementById('vCardVersionPageName' + i).value;
						var aAddressbookReadOnly = document.getElementById('readonlyCheckbox' + i).checked;
						var aAddressbookURL = document.getElementById('URLTextbox' + i).value;
						var aAddressbookUrnuuid = document.getElementById('urnuuidCheckbox' + i).checked;
						var aAddressbookUsername = aCheckbox.getAttribute('username');
						var aAddressbookValidationType = aCheckbox.getAttribute('validationType');
						var aAddressbookActionType = aCheckbox.getAttribute('actionType');
						var aAddressbookSourceDirPrefId = aCheckbox.getAttribute('sourceDirPrefId');
						var aAddressbookSourceCollected = (aCheckbox.getAttribute('sourceCollected') == 'true');
						if (cardbookUtils.isMyAccountRemote(myType)) {
							// the discover should be redone at every sync
							if (myType == 'APPLE') {
								aAddressbookURL = cardbookRepository.APPLE_API;
							}
							wdw_addressbooksAdd.gFinishParams.push({type: aAddressbookValidationType, url: aAddressbookURL, name: aAddressbookName, username: aAddressbookUsername, color: aAddressbookColor,
																	vcard: aAddressbookVCard, readonly: aAddressbookReadOnly, dirPrefId: aAddressbookId,
																	urnuuid: aAddressbookUrnuuid, DBcached: true, firstAction: false});
						} else if (myType == "LOCALDB") {
							wdw_addressbooksAdd.gFinishParams.push({type: aAddressbookValidationType, name: aAddressbookName, username: "", color: aAddressbookColor, vcard: aAddressbookVCard, readonly: aAddressbookReadOnly, dirPrefId: aAddressbookId,
																		urnuuid: aAddressbookUrnuuid, DBcached: true, firstAction: false});
						} else if (myType == "FILE" || myType == "DIRECTORY") {
							wdw_addressbooksAdd.gFinishParams.push({type: aAddressbookValidationType, actionType: aAddressbookActionType, file: wdw_addressbooksAdd.gFile, name: aAddressbookName, username: "",
																	color: aAddressbookColor, vcard: aAddressbookVCard, readonly: aAddressbookReadOnly, dirPrefId: aAddressbookId, urnuuid: aAddressbookUrnuuid, DBcached: false, firstAction: false});
						} else if (myType == "STANDARD") {
							if (window.arguments[0].action == "first") {
								var aFirstAction = true;
							} else {
								var aFirstAction = false;
							}
							wdw_addressbooksAdd.gFinishParams.push({type: "STANDARD", sourceDirPrefId: aAddressbookSourceDirPrefId,
																name: aAddressbookName, username: "", color: aAddressbookColor, vcard: aAddressbookVCard, readonly: aAddressbookReadOnly,
																dirPrefId: aAddressbookId, collected: aAddressbookSourceCollected,
																urnuuid: aAddressbookUrnuuid, DBcached: true, firstAction: aFirstAction});
						}
					}
					i++;
				} else {
					break;
				}
			}
		},

		setCanRewindFalse: function () {
			document.getElementById('addressbook-wizard').canRewind = false;
		},

		createAddressbook: function () {
			for (var i = 0; i < wdw_addressbooksAdd.gFinishParams.length; i++) {
				if (window.arguments[0].action == "search") {
					wdw_cardbook.modifySearchAddressbook(wdw_addressbooksAdd.gFinishParams[i].dirPrefId, wdw_addressbooksAdd.gFinishParams[i].name, wdw_addressbooksAdd.gFinishParams[i].color, wdw_addressbooksAdd.gFinishParams[i].vcard, wdw_addressbooksAdd.gFinishParams[i].readonly,
													wdw_addressbooksAdd.gFinishParams[i].urnuuid, wdw_addressbooksAdd.gFinishParams[i].searchDef);
				} else {
					if (wdw_addressbooksAdd.gFinishParams[i].type === "SEARCH") {
						var myFile = cardbookRepository.getRuleFile(wdw_addressbooksAdd.gFinishParams[i].dirPrefId);
						if (myFile.exists()) {
							myFile.remove(true);
						}
						myFile.create(Components.interfaces.nsIFile.NORMAL_FILE_TYPE, 420);
						cardbookSynchronization.writeContentToFile(myFile.path, wdw_addressbooksAdd.gFinishParams[i].searchDef, "UTF8");
						cardbookRepository.addAccountToRepository(wdw_addressbooksAdd.gFinishParams[i].dirPrefId, wdw_addressbooksAdd.gFinishParams[i].name, wdw_addressbooksAdd.gFinishParams[i].type, myFile.path, wdw_addressbooksAdd.gFinishParams[i].username, wdw_addressbooksAdd.gFinishParams[i].color,
																	wdw_addressbooksAdd.gFinishParams[i].enabled, true, wdw_addressbooksAdd.gFinishParams[i].vcard, false, null, null, wdw_addressbooksAdd.gFinishParams[i].DBcached, true, "60", true);
						cardbookComplexSearch.loadComplexSearchAccount(wdw_addressbooksAdd.gFinishParams[i].dirPrefId, true, "WINDOW");
					} else  if (cardbookUtils.isMyAccountRemote(wdw_addressbooksAdd.gFinishParams[i].type)) {
						cardbookRepository.addAccountToRepository(wdw_addressbooksAdd.gFinishParams[i].dirPrefId, wdw_addressbooksAdd.gFinishParams[i].name, wdw_addressbooksAdd.gFinishParams[i].type, wdw_addressbooksAdd.gFinishParams[i].url, wdw_addressbooksAdd.gFinishParams[i].username, wdw_addressbooksAdd.gFinishParams[i].color,
																	true, true, wdw_addressbooksAdd.gFinishParams[i].vcard, wdw_addressbooksAdd.gFinishParams[i].readonly, wdw_addressbooksAdd.gFinishParams[i].urnuuid,
																	wdw_addressbooksAdd.gFinishParams[i].DBcached, true, "60", true);
						cardbookSynchronization.syncAccount(wdw_addressbooksAdd.gFinishParams[i].dirPrefId);
					} else if (wdw_addressbooksAdd.gFinishParams[i].type === "STANDARD") {
						if (wdw_addressbooksAdd.gFinishParams[i].collected) {
							cardbookRepository.addAccountToCollected(wdw_addressbooksAdd.gFinishParams[i].dirPrefId);
						}
						cardbookRepository.addAccountToRepository(wdw_addressbooksAdd.gFinishParams[i].dirPrefId, wdw_addressbooksAdd.gFinishParams[i].name, "LOCALDB", "", wdw_addressbooksAdd.gFinishParams[i].username, wdw_addressbooksAdd.gFinishParams[i].color,
																	true, true, wdw_addressbooksAdd.gFinishParams[i].vcard, wdw_addressbooksAdd.gFinishParams[i].readonly, wdw_addressbooksAdd.gFinishParams[i].urnuuid,
																	wdw_addressbooksAdd.gFinishParams[i].DBcached, true, "60", true);
						cardbookSynchronization.initMultipleOperations(wdw_addressbooksAdd.gFinishParams[i].dirPrefId);
						cardbookRepository.cardbookDirRequest[wdw_addressbooksAdd.gFinishParams[i].dirPrefId]++;
						var myMode = "WINDOW";
						wdw_migrate.importCards(wdw_addressbooksAdd.gFinishParams[i].sourceDirPrefId, wdw_addressbooksAdd.gFinishParams[i].dirPrefId, wdw_addressbooksAdd.gFinishParams[i].name, wdw_addressbooksAdd.gFinishParams[i].vcard, myMode);
						cardbookSynchronization.waitForDirFinished(wdw_addressbooksAdd.gFinishParams[i].dirPrefId, wdw_addressbooksAdd.gFinishParams[i].name, myMode);
						// if the first proposed import of standard address books is finished OK
						// then set CardBook as exclusive
						if (wdw_addressbooksAdd.gFinishParams[i].firstAction) {
							cardbookPreferences.setBoolPref("extensions.cardbook.exclusive", true);
						}
					} else if (wdw_addressbooksAdd.gFinishParams[i].type === "LOCALDB") {
						cardbookRepository.addAccountToRepository(wdw_addressbooksAdd.gFinishParams[i].dirPrefId, wdw_addressbooksAdd.gFinishParams[i].name, wdw_addressbooksAdd.gFinishParams[i].type, "", wdw_addressbooksAdd.gFinishParams[i].username, wdw_addressbooksAdd.gFinishParams[i].color,
																	true, true, wdw_addressbooksAdd.gFinishParams[i].vcard, wdw_addressbooksAdd.gFinishParams[i].readonly, wdw_addressbooksAdd.gFinishParams[i].urnuuid,
																	wdw_addressbooksAdd.gFinishParams[i].DBcached, true, "60", true);
					} else if (wdw_addressbooksAdd.gFinishParams[i].type === "FILE") {
						cardbookRepository.addAccountToRepository(wdw_addressbooksAdd.gFinishParams[i].dirPrefId, wdw_addressbooksAdd.gFinishParams[i].name, wdw_addressbooksAdd.gFinishParams[i].type, wdw_addressbooksAdd.gFinishParams[i].file.path, wdw_addressbooksAdd.gFinishParams[i].username, wdw_addressbooksAdd.gFinishParams[i].color,
																	true, true, wdw_addressbooksAdd.gFinishParams[i].vcard, wdw_addressbooksAdd.gFinishParams[i].readonly, wdw_addressbooksAdd.gFinishParams[i].urnuuid,
																	wdw_addressbooksAdd.gFinishParams[i].DBcached, true, "60", true);
						cardbookSynchronization.initMultipleOperations(wdw_addressbooksAdd.gFinishParams[i].dirPrefId);
						cardbookRepository.cardbookFileRequest[wdw_addressbooksAdd.gFinishParams[i].dirPrefId]++;
						var myFile = wdw_addressbooksAdd.gFinishParams[i].file;
						if (wdw_addressbooksAdd.gFinishParams[i].actionType === "CREATEFILE") {
							if (myFile.exists()) {
								myFile.remove(true);
							}
							myFile.create(Components.interfaces.nsIFile.NORMAL_FILE_TYPE, 420);
						}
						var myMode = "WINDOW";
						cardbookSynchronization.loadFile(myFile, wdw_addressbooksAdd.gFinishParams[i].dirPrefId, myMode, "NOIMPORTFILE", "");
						cardbookSynchronization.waitForDirFinished(wdw_addressbooksAdd.gFinishParams[i].dirPrefId, wdw_addressbooksAdd.gFinishParams[i].name, myMode);
					} else if (wdw_addressbooksAdd.gFinishParams[i].type === "DIRECTORY") {
						var myDir = wdw_addressbooksAdd.gFinishParams[i].file;
						if (wdw_addressbooksAdd.gFinishParams[i].actionType === "CREATEDIRECTORY") {
							if (myDir.exists()) {
								var aListOfFileName = [];
								aListOfFileName = cardbookSynchronization.getFilesFromDir(myDir.path);
								if (aListOfFileName.length > 0) {
									var confirmTitle = cardbookRepository.strBundle.GetStringFromName("confirmTitle");
									var confirmMsg = cardbookRepository.strBundle.formatStringFromName("directoryDeletionConfirmMessage", [myDir.leafName], 1);
									if (Services.prompt.confirm(window, confirmTitle, confirmMsg)) {
										myDir.remove(true);
										try {
											myDir.create(Components.interfaces.nsIFile.DIRECTORY_TYPE, 0o774);
										}
										catch (e) {
											wdw_cardbooklog.updateStatusProgressInformation("cannot create directory : " + myDir.path + " : error : " + e, "Error");
											return;
										}
									} else {
										return;
									}
								}
							} else {
								try {
									myDir.create(Components.interfaces.nsIFile.DIRECTORY_TYPE, 0o774);
								}
								catch (e) {
									wdw_cardbooklog.updateStatusProgressInformation("cannot create directory : " + myDir.path + " : error : " + e, "Error");
									return;
								}
							}
						}
						cardbookRepository.addAccountToRepository(wdw_addressbooksAdd.gFinishParams[i].dirPrefId, wdw_addressbooksAdd.gFinishParams[i].name, wdw_addressbooksAdd.gFinishParams[i].type, wdw_addressbooksAdd.gFinishParams[i].file.path, wdw_addressbooksAdd.gFinishParams[i].username, wdw_addressbooksAdd.gFinishParams[i].color,
																	true, true, wdw_addressbooksAdd.gFinishParams[i].vcard, wdw_addressbooksAdd.gFinishParams[i].readonly, wdw_addressbooksAdd.gFinishParams[i].urnuuid,
																	wdw_addressbooksAdd.gFinishParams[i].DBcached, true, "60", true);
						cardbookSynchronization.initMultipleOperations(wdw_addressbooksAdd.gFinishParams[i].dirPrefId);
						cardbookRepository.cardbookDirRequest[wdw_addressbooksAdd.gFinishParams[i].dirPrefId]++;
						var myMode = "WINDOW";
						cardbookSynchronization.loadDir(myDir, wdw_addressbooksAdd.gFinishParams[i].dirPrefId, myMode, "NOIMPORTDIR", "");
						cardbookSynchronization.waitForDirFinished(wdw_addressbooksAdd.gFinishParams[i].dirPrefId, wdw_addressbooksAdd.gFinishParams[i].name, myMode);
					}
					cardbookUtils.formatStringForOutput("addressbookCreated", [wdw_addressbooksAdd.gFinishParams[i].name]);
					cardbookActions.addActivity("addressbookCreated", [wdw_addressbooksAdd.gFinishParams[i].name], "addItem");
					cardbookUtils.notifyObservers("addressbookCreated", wdw_addressbooksAdd.gFinishParams[i].dirPrefId);
				}
			}
		},

		cancelWizard: function () {
			for (var i = 0; i < wdw_addressbooksAdd.gRunningDirPrefId.length; i++) {
				cardbookPreferences.delBranch(wdw_addressbooksAdd.gRunningDirPrefId[i]);
				cardbookSynchronization.finishMultipleOperations(wdw_addressbooksAdd.gRunningDirPrefId[i]);
				cardbookSynchronization.stopDiscovery(wdw_addressbooksAdd.gRunningDirPrefId[i]);
			}
			for (var dirPrefId in wdw_addressbooksAdd.lTimerRefreshTokenAll) {
				try {
					wdw_addressbooksAdd.lTimerRefreshTokenAll[dirPrefId].cancel();
				} catch(e) {}
			}
			for (var dirPrefId in wdw_addressbooksAdd.lTimerDiscoveryAll) {
				try {
					wdw_addressbooksAdd.lTimerDiscoveryAll[dirPrefId].cancel();
				} catch(e) {}
			}
			document.getElementById('addressbook-wizard').canAdvance = false;
		},

		closeWizard: function () {
			wdw_addressbooksAdd.cancelWizard();
			wdw_addressbooksAdd.createAddressbook();
		},

	};

};
