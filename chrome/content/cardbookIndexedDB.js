if ("undefined" == typeof(cardbookIndexedDB)) {
	try {
		ChromeUtils.import("chrome://cardbook/content/cardbookRepository.js");
	}
	catch(e) {
		Components.utils.import("chrome://cardbook/content/cardbookRepository.js");
	}

	var cardbookIndexedDB = {

		// first step in the initial load data
		openDB: function () {
			// generic output when errors on DB
			cardbookRepository.cardbookDatabase.onerror = function(e) {
				wdw_cardbooklog.updateStatusProgressInformation("Database error : " + e.value, "Error");
			};

			var request = indexedDB.open(cardbookRepository.cardbookDatabaseName, cardbookRepository.cardbookDatabaseVersion);
		
			// when version changes
			// for the moment delete all and recreate one new empty
			request.onupgradeneeded = function(e) {
				var db = e.target.result;
				e.target.transaction.onerror = cardbookRepository.cardbookDatabase.onerror;
				
				if (db.objectStoreNames.contains("cards")) {
					db.deleteObjectStore("cards");
				}
				
				var store = db.createObjectStore("cards", {keyPath: "cbid", autoIncrement: false});
				store.createIndex("cacheuriIndex", "cacheuri", { unique: false });
			};

			// when success, call the observer for starting the load cache and maybe the sync
			request.onsuccess = function(e) {
				cardbookRepository.cardbookDatabase.db = e.target.result;
				cardbookUtils.notifyObservers("DBOpen");
			};

			// when error, call the observer for starting the load cache and maybe the sync
			request.onerror = function(e) {
				cardbookUtils.notifyObservers("DBOpen");
				cardbookRepository.cardbookDatabase.onerror(e);
			};
		},

		// add the contact to the cache only if it is missing
		addItemIfMissing: function (aDirPrefIdName, aCard) {
			var prefName = aDirPrefIdName;
			var card = aCard;
			var db = cardbookRepository.cardbookDatabase.db;
			var transaction = db.transaction(["cards"], "readonly");
			var store = transaction.objectStore("cards");
			var cursorRequest = store.get(aCard.cbid);
		
			cursorRequest.onsuccess = function(e) {
				if (!(e.target.result != null)) {
					cardbookIndexedDB.addItem(prefName, card);
				}
			};
			
			cursorRequest.onerror = cardbookRepository.cardbookDatabase.onerror;
		},
		
		// add or override the contact to the cache
		addItem: function (aDirPrefIdName, aCard) {
			var db = cardbookRepository.cardbookDatabase.db;
			var transaction = db.transaction(["cards"], "readwrite");
			var store = transaction.objectStore("cards");
			var cursorRequest = store.put(aCard);
			cursorRequest.onsuccess = function(e) {
				wdw_cardbooklog.updateStatusProgressInformationWithDebug2(aDirPrefIdName + " : debug mode : Contact " + aCard.fn + " written to DB");
			};
			
			cursorRequest.onerror = cardbookRepository.cardbookDatabase.onerror;
		},

		// delete the contact
		removeItem: function (aDirPrefIdName, aCard) {
			var db = cardbookRepository.cardbookDatabase.db;
			var transaction = db.transaction(["cards"], "readwrite");
			var store = transaction.objectStore("cards");
			var cursorRequest = store.delete(aCard.cbid);
			cursorRequest.onsuccess = function(e) {
				wdw_cardbooklog.updateStatusProgressInformationWithDebug2(aDirPrefIdName + " : debug mode : Contact " + aCard.fn + " deleted from DB");
			};
			
			cursorRequest.onerror = cardbookRepository.cardbookDatabase.onerror;
		},

		// once the DB is open, this is the second step for the AB
		// which use the DB caching
		loadItems: function (aDirPrefId, aDirPrefName, aMode, aCallback) {
			var cb = aCallback;
			var myMode = aMode;
			var db = cardbookRepository.cardbookDatabase.db;
			var keyRange = IDBKeyRange.bound(aDirPrefId, aDirPrefId + '\uffff');
			var transaction = db.transaction(["cards"], "readonly");
			var store = transaction.objectStore("cards");
			var countRequest = store.count(keyRange);
			var cursorRequest = store.openCursor(keyRange);
		
			transaction.oncomplete = function() {
				cb(aDirPrefId);
			};

			countRequest.onsuccess = function(e) {
				cardbookRepository.cardbookServerSyncTotal[aDirPrefId] = countRequest.result;
			};

			countRequest.onerror = function(e) {
				cardbookRepository.cardbookDatabase.onerror(e);
			};

			cursorRequest.onsuccess = function(e) {
				var result = e.target.result;
				if (result) {
					if (!result.value.deleted) {
						cardbookRepository.addCardToRepository(result.value, myMode, result.value.cacheuri);
						cardbookUtils.formatStringForOutput("cardLoadedFromCacheDB", [aDirPrefName, result.value.fn]);
					} else {
						if (cardbookRepository.cardbookFileCacheCards[aDirPrefId]) {
							cardbookRepository.cardbookFileCacheCards[aDirPrefId][result.value.cacheuri] = result.value;
						} else {
							cardbookRepository.cardbookFileCacheCards[aDirPrefId] = {};
							cardbookRepository.cardbookFileCacheCards[aDirPrefId][result.value.cacheuri] = result.value;
						}
					}
					cardbookRepository.cardbookServerSyncDone[aDirPrefId]++;
					result.continue();
				}
			};

			cursorRequest.onerror = function(e) {
				cardbookRepository.cardbookDatabase.onerror(e);
			};
		},

		// once the DB is open, this is the second step for the AB
		// which use the DB caching
		getItemByCacheuri: function (aCacheuri, aCallback, aParams) {
			var cb = aCallback;
			var params = aParams;
			var db = cardbookRepository.cardbookDatabase.db;
			var transaction = db.transaction(["cards"], "readonly");
			var store = transaction.objectStore("cards");
			var keyRange = IDBKeyRange.bound(aCacheuri, aCacheuri + '\uffff');
			var cursorRequest = store.index('cacheuriIndex').openCursor(keyRange);
		
			cursorRequest.onsuccess = function(e) {
				var result = e.target.result;
				if (result) {
					if (result.value.cacheuri == aCacheuri && result.value.dirPrefId == params.aDirPrefId) {
						cb(result.value, params);
					}
					result.continue();
				}
			};
			
			cursorRequest.onerror = function(e) {
				cardbookRepository.cardbookDatabase.onerror(e);
			};
		},

		// remove an account
		removeAccount: function (aDirPrefId, aDirPrefName) {
			var db = cardbookRepository.cardbookDatabase.db;
			var transaction = db.transaction(["cards"], "readwrite");
			var store = transaction.objectStore("cards");
			var keyRange = IDBKeyRange.bound(aDirPrefId, aDirPrefId + '\uffff');
			var cursorRequest = store.openCursor(keyRange);
		
			cursorRequest.onsuccess = function(e) {
				var result = e.target.result;
				if (result) {
					cardbookIndexedDB.removeItem(aDirPrefName, result.value);
					result.continue();
				}
			};
			
			cursorRequest.onerror = function(e) {
				cardbookRepository.cardbookDatabase.onerror(e);
			};
		},

		// when all contacts were loaded from the cache
		// tells that it is finished
		itemsComplete: function (aDirPrefId) {
			cardbookRepository.cardbookDBResponse[aDirPrefId]++;
		},
		
		// load all contacts for an addressbook
		loadDB: function (aDirPrefId, aDirPrefName, aMode) {
			cardbookIndexedDB.loadItems(aDirPrefId, aDirPrefName, aMode, cardbookIndexedDB.itemsComplete);
		},

		// first step for getting the undos
		openUndoDB: function () {
			// generic output when errors on DB
			cardbookRepository.cardbookActionsDatabase.onerror = function(e) {
				wdw_cardbooklog.updateStatusProgressInformation("Undo Database error : " + e.value, "Error");
			};

			var request = indexedDB.open(cardbookRepository.cardbookActionsDatabaseName, cardbookRepository.cardbookActionsDatabaseVersion);
		
			// when version changes
			// for the moment delete all and recreate one new empty
			request.onupgradeneeded = function(e) {
				var db = e.target.result;
				e.target.transaction.onerror = cardbookRepository.cardbookDatabase.onerror;
				
				if (db.objectStoreNames.contains("cardUndos")) {
					db.deleteObjectStore("cardUndos");
				}
				
				var store = db.createObjectStore("cardUndos", {keyPath: "undoId", autoIncrement: false});
			};

			request.onsuccess = function(e) {
				cardbookRepository.cardbookActionsDatabase.db = e.target.result;
				cardbookRepository.currentUndoId = Number(cardbookPreferences.getStringPref("extensions.cardbook.currentUndoId"));
				cardbookActions.setUndoAndRedoMenuAndButton();
			};

			request.onerror = function(e) {
				cardbookRepository.cardbookActionsDatabase.onerror(e);
			};
		},

		// set the menu label for the undo and redo menu entries
		setUndoAndRedoMenuAndButton: function (aMenuName, aButtonName, aUndoId) {
			// CardBook tab not open or db not open
			// for the standalone window it was unpossible to use the menus menu_undo et menu_redo
			// so menu_undo1 and menu_redo1 were used
			if (!document.getElementById(aMenuName)) {
				if (!document.getElementById(aMenuName + "1")) {
					return;
				} else {
					var myMenu = document.getElementById(aMenuName + "1");
				}
			} else {
				var myMenu = document.getElementById(aMenuName);
			}
			if (!cardbookRepository.cardbookActionsDatabase.db) {
				return;
			}
			var db = cardbookRepository.cardbookActionsDatabase.db;
			var transaction = db.transaction(["cardUndos"], "readonly");
			var store = transaction.objectStore("cardUndos");
			var keyRange = IDBKeyRange.bound(aUndoId, aUndoId);
			var cursorRequest = store.openCursor(keyRange);
		
			cursorRequest.onsuccess = function(e) {
				var result = e.target.result;
				if (result) {
					myMenu.removeAttribute('disabled');
					myMenu.setAttribute('label', cardbookRepository.strBundle.formatStringFromName(aMenuName + ".long.label", [result.value.undoMessage], 1));
					if (document.getElementById(aButtonName)) {
						document.getElementById(aButtonName).removeAttribute('disabled');
					}
				} else {
					myMenu.setAttribute('disabled', 'true');
					myMenu.setAttribute('label', cardbookRepository.strBundle.GetStringFromName(aMenuName + ".short.label"));
					if (document.getElementById(aButtonName)) {
						document.getElementById(aButtonName).setAttribute('disabled', 'true');
					}
				}
			};
			
			cursorRequest.onerror = function(e) {
				cardbookRepository.cardbookActionsDatabase.onerror(e);
			};
		},

		// remove an undo action
		removeUndoItem: function (aUndoId) {
			var db = cardbookRepository.cardbookActionsDatabase.db;
			var transaction = db.transaction(["cardUndos"], "readwrite");
			var store = transaction.objectStore("cardUndos");
			var keyRange = IDBKeyRange.upperBound(aUndoId);
			var cursorRequest = store.delete(keyRange);
			cursorRequest.onsuccess = function(e) {
				wdw_cardbooklog.updateStatusProgressInformationWithDebug2("debug mode : undo(s) less than " + aUndoId + " deleted from undoDB");
			};
			
			cursorRequest.onerror = cardbookRepository.cardbookActionsDatabase.onerror;
		},

		// add an undo action
		addUndoItem: function (aUndoId, aUndoCode, aUndoMessage, aOldCards, aNewCards) {
			var db = cardbookRepository.cardbookActionsDatabase.db;
			var transaction = db.transaction(["cardUndos"], "readwrite");
			var store = transaction.objectStore("cardUndos");
			var keyRange = IDBKeyRange.lowerBound(aUndoId);
			var cursorDeleteRequest = store.delete(keyRange);
			cursorDeleteRequest.onsuccess = function(e) {
				wdw_cardbooklog.updateStatusProgressInformationWithDebug2("debug mode : undo(s) more than " + aUndoId + " deleted from undoDB");

				var cursorAddRequest = store.put({undoId : aUndoId, undoCode : aUndoCode, undoMessage : aUndoMessage, oldCards: aOldCards, newCards: aNewCards});
				cursorAddRequest.onsuccess = function(e) {
					cardbookRepository.currentUndoId = aUndoId;
					cardbookActions.saveCurrentUndoId();
					cardbookActions.setUndoAndRedoMenuAndButton();
					wdw_cardbooklog.updateStatusProgressInformationWithDebug2("debug mode : undo " + aUndoId + " written to undoDB");
					var maxUndoChanges = cardbookPreferences.getStringPref("extensions.cardbook.maxUndoChanges");
					var undoIdToDelete = aUndoId - maxUndoChanges;
					if (undoIdToDelete > 0) {
						cardbookIndexedDB.removeUndoItem(undoIdToDelete);
					}
				};
				
				cursorAddRequest.onerror = cardbookRepository.cardbookActionsDatabase.onerror;
			};

			cursorDeleteRequest.onerror = cardbookRepository.cardbookActionsDatabase.onerror;
		},

		// do the undo action
		executeUndoItem: function () {
			var db = cardbookRepository.cardbookActionsDatabase.db;
			var transaction = db.transaction(["cardUndos"], "readonly");
			var store = transaction.objectStore("cardUndos");
			var keyRange = IDBKeyRange.bound(cardbookRepository.currentUndoId, cardbookRepository.currentUndoId);
			var cursorRequest = store.openCursor(keyRange);
		
			cursorRequest.onsuccess = function(e) {
				var result = e.target.result;
				if (result) {
					var myTopic = "undoActionDone";
					var myActionId = cardbookActions.startAction(myTopic, [result.value.undoMessage]);
					for (var i = 0; i < result.value.newCards.length; i++) {
						var myCard = result.value.newCards[i];
						wdw_cardbooklog.updateStatusProgressInformationWithDebug2("debug mode : executing undo " + cardbookRepository.currentUndoId + " deleting myCard.uid : "+ myCard.uid.toSource());
						cardbookRepository.deleteCards([myCard], myActionId);
					}
					for (var i = 0; i < result.value.oldCards.length; i++) {
						var myCard = result.value.oldCards[i];
						wdw_cardbooklog.updateStatusProgressInformationWithDebug2("debug mode : executing undo " + cardbookRepository.currentUndoId + " adding myCard.uid : "+ myCard.uid.toSource());
						cardbookRepository.saveCard({}, myCard, myActionId, false);
					}
					cardbookRepository.currentUndoId--;
					cardbookActions.saveCurrentUndoId();
					cardbookActions.setUndoAndRedoMenuAndButton();
					cardbookActions.endAction(myActionId);
				}
			};
			
			cursorRequest.onerror = function(e) {
				cardbookRepository.cardbookActionsDatabase.onerror(e);
			};
		},

		// do the redo action
		executeRedoItem: function () {
			var db = cardbookRepository.cardbookActionsDatabase.db;
			var transaction = db.transaction(["cardUndos"], "readonly");
			var store = transaction.objectStore("cardUndos");
			var nextUndoId = cardbookRepository.currentUndoId;
			nextUndoId++;
			var keyRange = IDBKeyRange.bound(nextUndoId, nextUndoId);
			var cursorRequest = store.openCursor(keyRange);
		
			cursorRequest.onsuccess = function(e) {
				var result = e.target.result;
				if (result) {
					var myTopic = "redoActionDone";
					var myActionId = cardbookActions.startAction(myTopic, [result.value.undoMessage]);
					for (var i = 0; i < result.value.oldCards.length; i++) {
						var myCard = result.value.oldCards[i];
						wdw_cardbooklog.updateStatusProgressInformationWithDebug2("debug mode : executing redo " + nextUndoId + " deleting myCard.uid : "+ myCard.uid.toSource());
						cardbookRepository.deleteCards([myCard], myActionId);
					}
					for (var i = 0; i < result.value.newCards.length; i++) {
						var myCard = result.value.newCards[i];
						wdw_cardbooklog.updateStatusProgressInformationWithDebug2("debug mode : executing redo " + nextUndoId + " adding myCard.uid : "+ myCard.uid.toSource());
						cardbookRepository.saveCard({}, myCard, myActionId, false);
					}
					cardbookRepository.currentUndoId++;
					cardbookActions.saveCurrentUndoId();
					cardbookActions.setUndoAndRedoMenuAndButton();
					cardbookActions.endAction(myActionId);
				}
			};
			
			cursorRequest.onerror = function(e) {
				cardbookRepository.cardbookActionsDatabase.onerror(e);
			};
		}

	};
};

