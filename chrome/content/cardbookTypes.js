if ("undefined" == typeof(cardbookTypes)) {
	try {
		ChromeUtils.import("resource://gre/modules/Services.jsm");
		ChromeUtils.import("chrome://cardbook/content/cardbookRepository.js");
	}
	catch(e) {
		Components.utils.import("resource://gre/modules/Services.jsm");
		Components.utils.import("chrome://cardbook/content/cardbookRepository.js");
	}


	var cardbookTypes = {
		
		allIMPPs: [],
		
		rebuildAllPGs: function (aCard) {
			let myPgNumber = 1;
			for (var i in cardbookRepository.multilineFields) {
				let myType = cardbookRepository.multilineFields[i];
				for (var j = 0; j < aCard[myType].length; j++) {
					let myTempString = aCard[myType][j][2];
					if (myTempString.startsWith("ITEM")) {
						aCard[myType][j][2] = "ITEM" + myPgNumber;
						myPgNumber++;
					}
				}
			}
			let myNewOthers = [];
			let myPGMap = {};
			for (var j = 0; j < aCard.others.length; j++) {
				let myTempString = aCard.others[j];
				var relative = []
				relative = myTempString.match(/^ITEM([0-9]*)\.(.*)/i);
				if (relative && relative[1] && relative[2]) {
					if (myPGMap[relative[1]]) {
						myNewOthers.push("ITEM" + myPGMap[relative[1]] + "." + relative[2]);
					} else {
						myNewOthers.push("ITEM" + myPgNumber + "." + relative[2]);
						myPGMap[relative[1]] = myPgNumber;
						myPgNumber++;
					}
				} else {
					myNewOthers.push(aCard.others[j]);
				}
			}
			aCard.others = JSON.parse(JSON.stringify(myNewOthers));
			return myPgNumber;
		},

		whichCodeTypeShouldBeChecked: function (aType, aDirPrefId, aSourceArray) {
			if (aSourceArray.length == 0) {
				return {result: "", isAPg: false, isAlreadyThere: false};
			} else {
				var ABType = cardbookPreferences.getType(aDirPrefId);
				var ABTypeFormat = cardbookRepository.getABTypeFormat(ABType);
				var match = false;
				for (var i = 0; i < cardbookRepository.cardbookCoreTypes[ABTypeFormat][aType].length && !match; i++) {
					var code = cardbookRepository.cardbookCoreTypes[ABTypeFormat][aType][i][0];
					var types = cardbookRepository.cardbookCoreTypes[ABTypeFormat][aType][i][1];
					var possibilities = types.split(";");
					for (var j = 0; j < possibilities.length && !match; j++) {
						var possibility = possibilities[j].split(",");
						for (var k = 0; k < aSourceArray.length; k++) {
							if (possibility.indexOf(aSourceArray[k].toUpperCase()) == -1) {
								break;
							} else if (possibility.indexOf(aSourceArray[k].toUpperCase()) != -1 && k == aSourceArray.length - 1 ) {
								// here we are sure that aSourceArray in included in possibility
								if (aSourceArray.length == possibility.length) {
									match = true;
									if (cardbookRepository.cardbookCoreTypes[ABTypeFormat][aType][i][2] && cardbookRepository.cardbookCoreTypes[ABTypeFormat][aType][i][2] == "PG") {
										return {result: code, isAPg: true, isAlreadyThere: true};
									} else {
										return {result: code, isAPg: false};
									}
								}
							}
						}
					}
				}
				return {result: aSourceArray[0], isAPg: true, isAlreadyThere: false};
			}
		},

		whichLabelTypeShouldBeChecked: function (aType, aDirPrefId, aSourceArray) {
			if (aSourceArray.length == 0) {
				return "";
			} else {
				var ABType = cardbookPreferences.getType(aDirPrefId);
				var ABTypeFormat = cardbookRepository.getABTypeFormat(ABType);
				var match = false;
				for (var i = 0; i < cardbookRepository.cardbookCoreTypes[ABTypeFormat][aType].length && !match; i++) {
					var code = cardbookRepository.cardbookCoreTypes[ABTypeFormat][aType][i][0];
					var types = cardbookRepository.cardbookCoreTypes[ABTypeFormat][aType][i][1];
					var possibilities = types.split(";");
					for (var j = 0; j < possibilities.length && !match; j++) {
						var possibility = possibilities[j].split(",");
						for (var k = 0; k < aSourceArray.length; k++) {
							if (possibility.indexOf(aSourceArray[k].toUpperCase()) == -1) {
								break;
							} else if (possibility.indexOf(aSourceArray[k].toUpperCase()) != -1 && k == aSourceArray.length - 1 ) {
								// here we are sure that aSourceArray in included in possibility
								if (aSourceArray.length == possibility.length) {
									match = true;
									return cardbookTypes.getTypeLabelFromTypeCode(ABTypeFormat, aType, code);
								}
							}
						}
					}
				}
				return aSourceArray[0];
			}
		},

		isMyCodePresent: function (aType, aCode, aABTypeFormat, aSourceArray) {
			var match = false;
			for (var i = 0; i < cardbookRepository.cardbookCoreTypes[aABTypeFormat][aType].length && !match; i++) {
				var code = cardbookRepository.cardbookCoreTypes[aABTypeFormat][aType][i][0];
				if (code != aCode) {
					continue;
				}
				var types = cardbookRepository.cardbookCoreTypes[aABTypeFormat][aType][i][1];
				var possibilities = types.split(";");
				for (var j = 0; j < possibilities.length && !match; j++) {
					var possibility = possibilities[j].split(",");
					for (var k = 0; k < aSourceArray.length; k++) {
						if (possibility.indexOf(aSourceArray[k].toUpperCase()) == -1) {
							break;
						} else if (possibility.indexOf(aSourceArray[k].toUpperCase()) != -1 && k == aSourceArray.length - 1 ) {
							// here we are sure that aSourceArray in included in possibility
							if (aSourceArray.length == possibility.length) {
								return true;
							}
						}
					}
				}
			}
			return false;
		},

		getTypeLabelFromTypeCode: function (aABType, aType, aTypeCode) {
			var prefResult = cardbookPreferences.getStringPref(cardbookPreferences.prefCardBookCustomTypes + aABType + "." + aType + "." + aTypeCode + ".value");
			if (prefResult != "") {
				return prefResult;
			} else {
				return cardbookRepository.strBundle.GetStringFromName(aTypeCode);
			}
			return aTypeCode;
		},

		getTypeDisabledFromTypeCode: function (aABType, aType, aTypeCode) {
			return cardbookPreferences.getBoolPref(cardbookPreferences.prefCardBookCustomTypes + aABType + "." + aType + "." + aTypeCode + ".disabled", false);
		},

		getTypes: function (aABType, aType, aResetToCore) {
			var result = [];
			for (let k = 0; k < cardbookRepository.cardbookCoreTypes[aABType][aType].length; k++) {
				var myCoreCodeType = cardbookRepository.cardbookCoreTypes[aABType][aType][k][0];
				var myDisabled = cardbookTypes.getTypeDisabledFromTypeCode(aABType, aType, myCoreCodeType);
				if (!myDisabled || aResetToCore) {
					var myLabel = cardbookTypes.getTypeLabelFromTypeCode(aABType, aType, myCoreCodeType);
					result.push([myLabel, myCoreCodeType]);
				}
			}
			if (!aResetToCore) {
				var count = {};
				var customTypes = Services.prefs.getChildList(cardbookPreferences.prefCardBookCustomTypes + aABType + "." + aType + ".", count);
				var tmpArray = [];
				for (let k = 0; k < customTypes.length; k++) {
					var tmpValue = customTypes[k].replace(cardbookPreferences.prefCardBookCustomTypes + aABType + "." + aType + ".", "");
					if (tmpValue.endsWith(".value")) {
						tmpArray.push(tmpValue.replace(".value", ""));
					}
				}
				for (let k = 0; k < tmpArray.length; k++) {
					var myCustomType = tmpArray[k];
					var isItACore = false;
					for (let l = 0; l < cardbookRepository.cardbookCoreTypes[aABType][aType].length; l++) {
						var myCoreCodeType = cardbookRepository.cardbookCoreTypes[aABType][aType][l][0];
						if (myCustomType == myCoreCodeType) {
							isItACore = true;
							break;
						}
					}
					if (!isItACore) {
						var myLabel = cardbookPreferences.getStringPref(cardbookPreferences.prefCardBookCustomTypes + aABType + "." + aType + "." + myCustomType + ".value");
						result.push([myLabel, myCustomType]);
					}
				}
			}
			return result;
		},

		getTypesFromDirPrefId: function (aType, aDirPrefId) {
			var result = [];
			if (aDirPrefId != null && aDirPrefId !== undefined && aDirPrefId != "") {
				var myABType = cardbookPreferences.getType(aDirPrefId);
				var myABTypeFormat = cardbookRepository.getABTypeFormat(myABType);
			} else {
				var myABTypeFormat = "CARDDAV";
			}
			result = cardbookTypes.getTypes(myABTypeFormat, aType, false);
			return cardbookUtils.sortMultipleArrayByString(result,0,1);
		},

		getIMPPLineForCode: function (aCode) {
			var serviceLine = [];
			myPrefResults = cardbookPreferences.getAllIMPPs();
			for (var i = 0; i < myPrefResults.length; i++) {
				if (aCode.toLowerCase() == myPrefResults[i][0].toLowerCase()) {
					serviceLine = [myPrefResults[i][0], myPrefResults[i][1], myPrefResults[i][2]];
					break;
				}
			}
			return serviceLine;
		},

		getIMPPLineForProtocol: function (aProtocol) {
			var serviceLine = [];
			myPrefResults = cardbookPreferences.getAllIMPPs();
			for (var i = 0; i < myPrefResults.length; i++) {
				if (aProtocol.toLowerCase() == myPrefResults[i][2].toLowerCase()) {
					serviceLine = [myPrefResults[i][0], myPrefResults[i][1], myPrefResults[i][2]];
					break;
				}
			}
			return serviceLine;
		},

		getIMPPCode: function (aInputTypes) {
			var serviceCode = "";
			for (var j = 0; j < aInputTypes.length; j++) {
				serviceCode = aInputTypes[j].replace(/^X-SERVICE-TYPE=/i, "");
				if (serviceCode != aInputTypes[j]) {
					break;
				} else {
					serviceCode = "";
				}
			}
			return serviceCode;
		},

		getIMPPProtocol: function (aCardValue) {
			var serviceProtocol = "";
			if (aCardValue[0].indexOf(":") >= 0) {
				serviceProtocol = aCardValue[0].split(":")[0];
			}
			return serviceProtocol;
		},

		loadIMPPs: function (aArray) {
			var myPrefResults = [];
			myPrefResults = cardbookPreferences.getAllIMPPs();
			var serviceCode = "";
			var serviceProtocol = "";
			for (var i = 0; i < aArray.length; i++) {
				serviceCode = cardbookTypes.getIMPPCode(aArray[i][1]);
				serviceProtocol = cardbookTypes.getIMPPProtocol(aArray[i][0]);
				if (serviceCode != "" || serviceProtocol != "") {
					var found = false;
					for (var j = 0; j < myPrefResults.length; j++) {
						if (serviceCode != "") {
							if (myPrefResults[j][0].toLowerCase() == serviceCode.toLowerCase()) {
								found = true;
								break;
							}
						} else if (serviceProtocol != "") {
							if (myPrefResults[j][2].toLowerCase() == serviceProtocol.toLowerCase()) {
								found = true;
								break;
							}
						}
					}
					if (!found) {
						if (serviceCode == "") {
							myPrefResults.push([serviceProtocol, serviceProtocol, serviceProtocol]);
						} else if (serviceProtocol == "") {
							myPrefResults.push([serviceCode, serviceCode, serviceCode]);
						} else {
							myPrefResults.push([serviceCode, serviceCode, serviceProtocol]);
						}
					}
				}
			}
			cardbookTypes.allIMPPs = JSON.parse(JSON.stringify(myPrefResults));
			cardbookTypes.allIMPPs = cardbookUtils.sortMultipleArrayByString(cardbookTypes.allIMPPs,1,1);
		},

		getTypeForLine: function (aType, aIndex) {
			var myLineResult = [];
			var myLineTypeResult = [];
			
			var myPrefButton = document.getElementById(aType + '_' + aIndex + '_PrefImage');
			if (document.getElementById('versionTextBox').value === "4.0") {
				if (myPrefButton.getAttribute('haspref')) {
					var aPrefWeightBoxValue = document.getElementById(aType + '_' + aIndex + '_prefWeightBox').value;
					if (aPrefWeightBoxValue != null && aPrefWeightBoxValue !== undefined && aPrefWeightBoxValue != "") {
						myLineTypeResult.push("PREF=" + aPrefWeightBoxValue);
					} else {
						myLineTypeResult.push("PREF");
					}
				}
			} else {
				if (myPrefButton.getAttribute('haspref')) {
					myLineTypeResult.push("TYPE=PREF");
				}
			}

			var myLineOtherType = document.getElementById(aType + '_' + aIndex + '_othersTypesBox').value;
			if (myLineOtherType != null && myLineOtherType !== undefined && myLineOtherType != "") {
				myLineTypeResult = myLineTypeResult.concat(myLineOtherType.split(','));
			}
			
			var myLineTypeType = [];
			var myPanel = document.getElementById(aType + '_' + aIndex + '_PanelType');
			var myOutputPg = [];
			var myPgName = "";
			if (myPanel) {
				if (myPanel.types.length != 0) {
					if (myPanel.types[1] == "PG") {
						myOutputPg = [ "X-ABLABEL:" + myPanel.types[0] ];
						myPgName = "ITEM1";
					} else {
						myLineTypeType.push("TYPE=" + myPanel.types[0]);
					}
				}
			}
			if (myLineTypeType.length > 0) {
				myLineTypeResult = myLineTypeResult.concat(myLineTypeType);
				myLineTypeResult = cardbookUtils.unescapeArray(cardbookUtils.formatTypes(cardbookUtils.escapeArray(myLineTypeResult)));
			}
			
			if (aType == "adr") {
				var j = 0;
				var myLineTypeValue = [];
				while (true) {
					if (document.getElementById(aType + '_' + aIndex + '_valueBox_' + j)) {
						var myTypeValue = document.getElementById(aType + '_' + aIndex + '_valueBox_' + j).value.replace(/\\n/g, "\n").trim();
						myLineTypeValue.push(myTypeValue);
						j++;
					} else {
						break;
					}
				}
			} else {
				var myLineTypeValue = [document.getElementById(aType + '_' + aIndex + '_valueBox').value.trim()];
			}
			
			if (aType == "impp" && document.getElementById(aType + '_' + aIndex + '_menulistIMPP').selectedItem) {
				return [myLineTypeValue, myLineTypeResult, myPgName, myOutputPg, document.getElementById(aType + '_' + aIndex + '_menulistIMPP').selectedItem.value];
			} else {
				return [myLineTypeValue, myLineTypeResult, myPgName, myOutputPg, ""];
			}
		},

		getIMPPTypes: function () {
			var i = 0;
			var myResult = [];
			while (true) {
				if (document.getElementById('impp_' + i + '_hbox')) {
					var lineResult = cardbookTypes.getTypeForLine('impp', i);
					if (lineResult[0].join("") != "") {
						function removeServiceType(element) {
							return (element == element.replace(/^X-SERVICE-TYPE=/i, ""));
						}
						lineResult[1] = lineResult[1].filter(removeServiceType);
						lineResult[1].push("X-SERVICE-TYPE=" + lineResult[4]);

						var myValue = lineResult[0].join(" ");
						serviceLine = cardbookTypes.getIMPPLineForCode(lineResult[4])
						if (serviceLine[0]) {
							var myRegexp = new RegExp("^" + serviceLine[2] + ":");
							myValue = myValue.replace(myRegexp, "");
							myValue = serviceLine[2] + ":" + myValue;
						}
						myResult.push([[myValue], lineResult[1], "", []]);
					}
					i++;
				} else {
					break;
				}
			}
			return myResult;
		},

		getAllTypes: function (aType, aRemoveNull) {
			var i = 0;
			var myResult = [];
			while (true) {
				if (document.getElementById(aType + '_' + i + '_hbox')) {
					var lineResult = cardbookTypes.getTypeForLine(aType, i);
					if (lineResult[0].join("") != "" || !aRemoveNull) {
						myResult.push(lineResult);
					}
					i++;
				} else {
					break;
				}
			}
			return myResult;
		},

		getAllEvents: function (aRemoveNull) {
			var myType = "event";
			var i = 0;
			var myResult = [];
			while (true) {
				if (document.getElementById(myType + '_' + i + '_hbox')) {
					var myPrefButton = document.getElementById(myType + '_' + i + '_PrefImage');
					var dateResult = document.getElementById(myType + '_' + i + '_valueDateBox').value;
					var nameResult = document.getElementById(myType + '_' + i + '_valueBox').value;
					if ((nameResult != "" && dateResult != "") || !aRemoveNull) {
						myResult.push([dateResult, nameResult, myPrefButton.getAttribute('haspref')]);
					}
					i++;
				} else {
					break;
				}
			}
			return myResult;
		},

		disableButtons: function (aType, aIndex) {
			if (aIndex == 0) {
				if (document.getElementById(aType + '_' + aIndex + '_valueBox').value == "") {
					document.getElementById(aType + '_' + aIndex + '_removeButton').disabled = true;
					document.getElementById(aType + '_' + aIndex + '_addButton').disabled = true;
				} else {
					document.getElementById(aType + '_' + aIndex + '_addButton').disabled = false;
					document.getElementById(aType + '_' + aIndex + '_removeButton').disabled = false;
				}
			} else {
				document.getElementById(aType + '_0_removeButton').disabled = false;
				for (var i = 0; i < aIndex; i++) {
					document.getElementById(aType + '_' + i + '_addButton').disabled = true;
					document.getElementById(aType + '_' + i + '_downButton').disabled = false;
				}
			}
			document.getElementById(aType + '_' + aIndex + '_downButton').disabled = true;
			document.getElementById(aType + '_0_upButton').disabled = true;
		},

		findNextLine: function (aType) {
			var i = 0;
			while (true) {
				if (document.getElementById(aType + '_' + i + '_hbox') || document.getElementById(aType + '_' + i + '_row')) {
					i++;
				} else {
					return i;
				}
			}
		},

		constructDynamicRows: function (aType, aArray, aVersion) {
			var start = cardbookTypes.findNextLine(aType);
			for (var i = 0; i < aArray.length; i++) {
				cardbookTypes.loadDynamicTypes(aType, i+start, aArray[i][1], aArray[i][2], aArray[i][3], aArray[i][0], aVersion);
			}
			if (aArray.length == 0) {
				cardbookTypes.loadDynamicTypes(aType, start, [], "", [], [""], aVersion);
			}
		},

		constructDynamicEventsRows: function (aType, aEventType, aVersion) {
			var start = cardbookTypes.findNextLine(aType);
			for (var i = 0; i < aEventType.length; i++) {
				cardbookTypes.loadDynamicEventsTypes(aType, i+start, aEventType[i], aVersion);
			}
			if (aEventType.length == 0) {
				cardbookTypes.loadDynamicEventsTypes(aType, start, ["", ""], aVersion);
			}
		},


		constructStaticRows: function (aDirPrefId, aType, aArray, aVersion, aFollowLink) {
			for (var i = 0; i < aArray.length; i++) {
				cardbookTypes.loadStaticTypes(aDirPrefId, aType, i, aArray[i][1], aArray[i][2], aArray[i][3], aArray[i][0], aVersion, aFollowLink);
			}
		},

		constructStaticEventsRows: function (aDirPrefId, aEventType, aVersion) {
			for (var i = 0; i < aEventType.length; i++) {
				cardbookTypes.loadStaticEventsTypes(aDirPrefId, "event", i, aEventType[i], aVersion);
			}
		},

		display40: function (aVersion) {
			var usePreferenceValue = cardbookPreferences.getBoolPref("extensions.cardbook.usePreferenceValue");
			for (var i in cardbookRepository.multilineFields) {
				let myType = cardbookRepository.multilineFields[i];
				if (document.getElementById(myType + 'Groupbox')) {
					var j = 0;
					while (true) {
						if (document.getElementById(myType + '_' + j + '_prefWeightBox')) {
							var myPrefWeightBoxLabel = document.getElementById(myType + '_' + j + '_prefWeightBoxLabel');
							var myPrefWeightBox = document.getElementById(myType + '_' + j + '_prefWeightBox');
							if (aVersion === "4.0" && usePreferenceValue) {
								myPrefWeightBoxLabel.removeAttribute('hidden');
								myPrefWeightBox.removeAttribute('hidden');
							} else {
								myPrefWeightBoxLabel.setAttribute('hidden', 'true');
								myPrefWeightBox.setAttribute('hidden', 'true');
							}
							if (document.getElementById(myType + '_' + j + '_PrefImage').getAttribute('haspref')) {
								myPrefWeightBoxLabel.removeAttribute('readonly');
							} else {
								myPrefWeightBoxLabel.setAttribute('readonly', 'true');
							}
							j++;
						} else {
							break;
						}
					}
				}
			}
		},

		constructOrg: function (aReadOnly, aOrgValue, aTitleValue, aRoleValue) {
			var aOrigBox = document.getElementById('orgRows');
			var orgStructure = cardbookPreferences.getStringPref("extensions.cardbook.orgStructure");
			var currentRow;
			if (orgStructure != "") {
				var myOrgStructure = cardbookUtils.unescapeArray(cardbookUtils.escapeString(orgStructure).split(";"));
				var myOrgValue = cardbookUtils.unescapeArray(cardbookUtils.escapeString(aOrgValue).split(";"));
				for (var i = 0; i < myOrgStructure.length; i++) {
					var myValue = "";
					if (myOrgValue[i]) {
						myValue = myOrgValue[i];
					}
					if (aReadOnly) {
						if (myValue != "") {
							currentRow = cardbookElementTools.addGridRow(aOrigBox, 'orgRow_' + i, {align: 'center'});
							cardbookElementTools.addLabel(currentRow, 'orgLabel_' + i, myOrgStructure[i], 'orgTextBox_' + i, {class: 'header'});
							cardbookElementTools.addTextbox(currentRow, 'orgTextBox_' + i, myValue, {flex: '1', readonly: 'true'});
						}
					} else {
						currentRow = cardbookElementTools.addGridRow(aOrigBox, 'orgRow_' + i, {align: 'center'});
						cardbookElementTools.addLabel(currentRow, 'orgLabel_' + i, myOrgStructure[i], 'orgTextBox_' + i, {class: 'header'});
						var myTextBox = cardbookElementTools.addTextbox(currentRow, 'orgTextBox_' + i, myValue, {flex: '1', type: 'autocomplete', autocompletesearch: 'form-history', autocompletesearchparam: 'orgTextBox_' + i, class:'padded'});
						myTextBox.addEventListener("input", wdw_cardEdition.setDisplayName, false);
					}
				}
			} else {
				var myOrgValue = cardbookUtils.unescapeString(cardbookUtils.escapeString(aOrgValue));
				if (aReadOnly) {
					if (myOrgValue != "") {
						currentRow = cardbookElementTools.addGridRow(aOrigBox, 'orgRow_0', {align: 'center'});
						cardbookElementTools.addLabel(currentRow, 'orgLabel', cardbookRepository.strBundle.GetStringFromName("orgLabel"), 'orgTextBox_0', {class: 'header'});
						cardbookElementTools.addTextbox(currentRow, 'orgTextBox_0', myOrgValue, {flex: '1', readonly: 'true'});
					}
				} else {
					currentRow = cardbookElementTools.addGridRow(aOrigBox, 'orgRow_0', {align: 'center'});
					cardbookElementTools.addLabel(currentRow, 'orgLabel', cardbookRepository.strBundle.GetStringFromName("orgLabel"), 'orgTextBox_0', {class: 'header'});
					var myTextBox = cardbookElementTools.addTextbox(currentRow, 'orgTextBox_0', myOrgValue, {flex: '1', type: 'autocomplete', autocompletesearch: 'form-history', autocompletesearchparam: 'orgTextBox_0', class:'padded'});
					myTextBox.addEventListener("input", wdw_cardEdition.setDisplayName, false);
				}
			}
			if (aReadOnly) {
				if (aTitleValue != "") {
					currentRow = cardbookElementTools.addGridRow(aOrigBox, 'titleRow', {align: 'center'});
					cardbookElementTools.addLabel(currentRow, 'titleLabel', cardbookRepository.strBundle.GetStringFromName("titleLabel"), 'titleTextBox', {class: 'header'});
					cardbookElementTools.addTextbox(currentRow, 'titleTextBox', aTitleValue, {flex: '1', readonly: 'true'});
				}
				if (aRoleValue != "") {
					currentRow = cardbookElementTools.addGridRow(aOrigBox, 'roleRow', {align: 'center'});
					cardbookElementTools.addLabel(currentRow, 'roleLabel', cardbookRepository.strBundle.GetStringFromName("roleLabel"), 'roleTextBox', {class: 'header'});
					cardbookElementTools.addTextbox(currentRow, 'roleTextBox', aRoleValue, {flex: '1', readonly: 'true'});
				}
			} else {
				currentRow = cardbookElementTools.addGridRow(aOrigBox, 'titleRow', {align: 'center'});
				cardbookElementTools.addLabel(currentRow, 'titleLabel', cardbookRepository.strBundle.GetStringFromName("titleLabel"), 'titleTextBox', {class: 'header'});
				var myTextBox = cardbookElementTools.addTextbox(currentRow, 'titleTextBox', aTitleValue, {flex: '1', type: 'autocomplete', autocompletesearch: 'form-history', autocompletesearchparam: 'titleTextBox', class:'padded'});
				myTextBox.addEventListener("input", wdw_cardEdition.setDisplayName, false);
				currentRow = cardbookElementTools.addGridRow(aOrigBox, 'roleRow', {align: 'center'});
				cardbookElementTools.addLabel(currentRow, 'roleLabel', cardbookRepository.strBundle.GetStringFromName("roleLabel"), 'roleTextBox', {class: 'header'});
				var myTextBox = cardbookElementTools.addTextbox(currentRow, 'roleTextBox', aRoleValue, {flex: '1', type: 'autocomplete', autocompletesearch: 'form-history', autocompletesearchparam: 'roleTextBox', class:'padded'});
				myTextBox.addEventListener("input", wdw_cardEdition.setDisplayName, false);
			}
		},

		constructCustom: function (aReadOnly, aType, aOtherValue) {
			var aOrigBox = document.getElementById(aType + 'Rows');

			var othersTemp = JSON.parse(JSON.stringify(aOtherValue));
			var result = [];
			result = cardbookRepository.customFields[aType];
			for (let i = 0; i < result.length; i++) {
				var myCode = result[i][0];
				var myLabel = result[i][1];
				var myField = 'customField' + i + aType;
				var myValue = '';
				for (var j = 0; j < othersTemp.length; j++) {
					var localDelim1 = othersTemp[j].indexOf(":",0);
					var myTestCode = othersTemp[j].substr(0,localDelim1);
					if (myCode == myTestCode) {
						myValue = othersTemp[j].substr(localDelim1+1,othersTemp[j].length);
						break;
					}
				}
				var dummy = othersTemp.splice(j,1);
				j--;
				if (aReadOnly) {
					if (myValue != "") {
						currentRow = cardbookElementTools.addGridRow(aOrigBox, myField + 'Row', {align: 'center'});
						cardbookElementTools.addLabel(currentRow, myField + 'Label', myLabel, myField + 'TextBox', {class: 'header'});
						cardbookElementTools.addTextbox(currentRow, myField + 'TextBox', myValue, {flex: '1', readonly: 'true'});
					}
				} else {
					currentRow = cardbookElementTools.addGridRow(aOrigBox, myField + 'Row', {align: 'center'});
					cardbookElementTools.addLabel(currentRow, myField + 'Label', myLabel, myField + 'TextBox', {class: 'header'});
					cardbookElementTools.addTextbox(currentRow, myField + 'TextBox', myValue, {flex: '1'});
				}
			}
			return othersTemp;
		},

		loadDynamicTypes: function (aType, aIndex, aInputTypes, aPgName, aPgType, aCardValue, aVersion) {
			var aOrigBox = document.getElementById(aType + 'Groupbox');
			
			if (aIndex == 0) {
				cardbookElementTools.addCaption(aType, aOrigBox);
			}
			
			var aHBox = cardbookElementTools.addHBox(aType, aIndex, aOrigBox);

			var myInputTypes = [];
			myInputTypes = cardbookUtils.getOnlyTypesFromTypes(aInputTypes);
			var myOthersTypes = cardbookUtils.getNotTypesFromTypes(aInputTypes);
			
			var aPrefButton = cardbookElementTools.addPrefStar(aHBox, aType, aIndex, cardbookUtils.getPrefBooleanFromTypes(aInputTypes))
			
			cardbookElementTools.addLabel(aHBox, aType + '_' + aIndex + '_prefWeightBoxLabel', cardbookPreferences.getPrefValueLabel(), aType + '_' + aIndex + '_prefWeightBox', {tooltip: cardbookRepository.strBundle.GetStringFromName("prefWeightTooltip")});
			cardbookElementTools.addTextbox(aHBox, aType + '_' + aIndex + '_prefWeightBox', cardbookUtils.getPrefValueFromTypes(aInputTypes, document.getElementById('versionTextBox').value), {size: "5"});
			if (aPrefButton.getAttribute('haspref')) {
				document.getElementById(aType + '_' + aIndex + '_prefWeightBoxLabel').disabled = false;
				document.getElementById(aType + '_' + aIndex + '_prefWeightBox').disabled = false;
			} else {
				document.getElementById(aType + '_' + aIndex + '_prefWeightBoxLabel').disabled = true;
				document.getElementById(aType + '_' + aIndex + '_prefWeightBox').disabled = true;
			}

			var usePreferenceValue = cardbookPreferences.getBoolPref("extensions.cardbook.usePreferenceValue");
			if (document.getElementById('versionTextBox').value === "4.0" && usePreferenceValue) {
				document.getElementById(aType + '_' + aIndex + '_prefWeightBoxLabel').removeAttribute('hidden');
				document.getElementById(aType + '_' + aIndex + '_prefWeightBox').removeAttribute('hidden');
			} else {
				document.getElementById(aType + '_' + aIndex + '_prefWeightBoxLabel').setAttribute('hidden', 'true');
				document.getElementById(aType + '_' + aIndex + '_prefWeightBox').setAttribute('hidden', 'true');
			}

			cardbookElementTools.addTextbox(aHBox, aType + '_' + aIndex + '_othersTypesBox', myOthersTypes, {hidden: "true"});

			if (aType != "impp") {
				var myCheckedArrayTypes = [];
				if (aPgType.length != 0 && aPgName != "") {
					let found = false;
					for (var j = 0; j < aPgType.length; j++) {
						let tmpArray = aPgType[j].split(":");
						if (tmpArray[0] == "X-ABLABEL") {
							cardbookElementTools.addMenuTypelist(aHBox, aType, aIndex, [tmpArray[1]]);
							myCheckedArrayTypes.push(tmpArray[1]);
							found = true;
							break;
						}
					}
					if (!found) {
						for (var j = 0; j < myInputTypes.length; j++) {
							myCheckedArrayTypes.push(myInputTypes[j]);
						}
						cardbookElementTools.addMenuTypelist(aHBox, aType, aIndex, myCheckedArrayTypes);
					}
				} else {
					for (var j = 0; j < myInputTypes.length; j++) {
						myCheckedArrayTypes.push(myInputTypes[j]);
					}
					cardbookElementTools.addMenuTypelist(aHBox, aType, aIndex, myCheckedArrayTypes);
				}
			}

			if (aType == "impp") {
				var serviceCode = cardbookTypes.getIMPPCode(aInputTypes);
				var serviceProtocol = cardbookTypes.getIMPPProtocol(aCardValue);
				cardbookElementTools.addMenuIMPPlist(aHBox, aType, aIndex, cardbookTypes.allIMPPs, serviceCode, serviceProtocol);
				var myValue = aCardValue.join(" ");
				if (serviceCode != "") {
					var serviceLine = [];
					serviceLine = cardbookTypes.getIMPPLineForCode(serviceCode)
					if (serviceLine[0]) {
						var myRegexp = new RegExp("^" + serviceLine[2] + ":");
						myValue = myValue.replace(myRegexp, "");
					}
				} else if (serviceProtocol != "") {
					var serviceLine = [];
					serviceLine = cardbookTypes.getIMPPLineForProtocol(serviceProtocol)
					if (serviceLine[0]) {
						var myRegexp = new RegExp("^" + serviceLine[2] + ":");
						myValue = myValue.replace(myRegexp, "");
					}
				}
				cardbookElementTools.addKeyTextbox(aHBox, aType + '_' + aIndex + '_valueBox', myValue, {flex: "1"}, aIndex);
			} else if (aType == "adr") {
				var myTmpArray = [];
				for (var i = 0; i < aCardValue.length; i++) {
					if (aCardValue[i] != "") {
						myTmpArray.push(aCardValue[i].replace(/\n/g, " "));
					}
				}
				cardbookElementTools.addKeyTextbox(aHBox, aType + '_' + aIndex + '_valueBox', myTmpArray.join(" "), {flex: "1", context: "adrCardContextMenu"}, aIndex);
			} else {
				cardbookElementTools.addKeyTextbox(aHBox, aType + '_' + aIndex + '_valueBox', cardbookUtils.cleanArray(aCardValue).join(" "), {flex: "1"}, aIndex);
			}

			if (aType == "adr") {
				function fireEditAdrOnClick(aEvent) {
					if (aEvent.button == 0) {
						var myIdArray = this.id.split('_');
						var myTempResult = cardbookTypes.getTypeForLine(aType, aIndex);
						if (myTempResult.length == 0) {
							var adrLine = [ ["", "", "", "", "", "", ""], [""], "", [""] ];
						} else {
							var adrLine = myTempResult;
						}
						wdw_cardEdition.openAdrPanel(adrLine, myIdArray);
					}
				};
				document.getElementById(aType + '_' + aIndex + '_valueBox').addEventListener("click", fireEditAdrOnClick, false);
				function fireEditAdrOnInput() {
					var myIdArray = this.id.split('_');
					var myTempResult = cardbookTypes.getTypeForLine(aType, aIndex);
					if (myTempResult.length == 0) {
						var adrLine = [ ["", "", "", "", "", "", ""], [""], "", [""] ];
					} else {
						var adrLine = myTempResult;
					}
					wdw_cardEdition.openAdrPanel(adrLine, myIdArray);
				};
				document.getElementById(aType + '_' + aIndex + '_valueBox').addEventListener("keydown", fireEditAdrOnInput, false);

				let i = 0;
				while ( i < 7 ) {
					if (aCardValue[i]) {
						cardbookElementTools.addTextbox(aHBox, aType + '_' + aIndex + '_valueBox_' + i, aCardValue[i].replace(/\n/g, "\\n"), {hidden: "true"});
					} else {
						cardbookElementTools.addTextbox(aHBox, aType + '_' + aIndex + '_valueBox_' + i, "", {hidden: "true"});
					}
					i++;
				}
			} else if (aType == "tel") {
				function fireInputTel(event) {
					var myValidationButton = document.getElementById(aType + '_' + aIndex + '_validateButton');
					var tel = PhoneNumber.Parse(this.value, wdw_cardEdition.cardRegion);
					if (tel && tel.internationalFormat && this.value == tel.internationalFormat) {
						myValidationButton.setAttribute('label', '✔');
						myValidationButton.setAttribute('tooltiptext', cardbookRepository.strBundle.GetStringFromName("validatedEntryTooltip"));
					} else {
						myValidationButton.setAttribute('label', '!');
						myValidationButton.setAttribute('tooltiptext', cardbookRepository.strBundle.GetStringFromName("notValidatedEntryTooltip"));
					}
				};
				document.getElementById(aType + '_' + aIndex + '_valueBox').addEventListener("input", fireInputTel, false);
			}
		
			if (aType == "tel") {
				function fireValidateTelButton(event) {
					if (document.getElementById(this.id).disabled) {
						return;
					}
					var myIdArray = this.id.split('_');
					var myTelTextBox = document.getElementById(aType + '_' + aIndex + '_valueBox');
					var tel = PhoneNumber.Parse(myTelTextBox.value, wdw_cardEdition.cardRegion);
					if (tel && tel.internationalFormat) {
						myTelTextBox.value = tel.internationalFormat;
						this.setAttribute('label', '✔');
						this.setAttribute('tooltiptext', cardbookRepository.strBundle.GetStringFromName("validatedEntryTooltip"));
					} else {
						this.setAttribute('label', '!');
						this.setAttribute('tooltiptext', cardbookRepository.strBundle.GetStringFromName("notValidatedEntryTooltip"));
					}
				};
				var myTelTextBoxValue = document.getElementById(aType + '_' + aIndex + '_valueBox').value;
				if (myTelTextBoxValue == "") {
					cardbookElementTools.addEditButton(aHBox, aType, aIndex, 'noValidated', 'validate', fireValidateTelButton);
				} else {
					var tel = PhoneNumber.Parse(myTelTextBoxValue, wdw_cardEdition.cardRegion);
					if (tel && tel.internationalFormat) {
						cardbookElementTools.addEditButton(aHBox, aType, aIndex, 'validated', 'validate', fireValidateTelButton);
					} else {
						cardbookElementTools.addEditButton(aHBox, aType, aIndex, 'notValidated', 'validate', fireValidateTelButton);
					}
				}
			}
			
			function fireUpButton(event) {
				if (document.getElementById(this.id).disabled) {
					return;
				}
				var myAllValuesArray = cardbookTypes.getAllTypes(aType, false);
				if (myAllValuesArray.length <= 1) {
					return;
				}
				var temp = myAllValuesArray[aIndex*1-1];
				myAllValuesArray[aIndex*1-1] = myAllValuesArray[aIndex];
				myAllValuesArray[aIndex] = temp;
				cardbookElementTools.deleteRowsType(aType);
				cardbookTypes.constructDynamicRows(aType, myAllValuesArray, aVersion);
			};
			cardbookElementTools.addEditButton(aHBox, aType, aIndex, 'up', 'up', fireUpButton);
			
			function fireDownButton(event) {
				if (document.getElementById(this.id).disabled) {
					return;
				}
				var myAllValuesArray = cardbookTypes.getAllTypes(aType, false);
				if (myAllValuesArray.length <= 1) {
					return;
				}
				var temp = myAllValuesArray[aIndex*1+1];
				myAllValuesArray[aIndex*1+1] = myAllValuesArray[aIndex];
				myAllValuesArray[aIndex] = temp;
				cardbookElementTools.deleteRowsType(aType);
				cardbookTypes.constructDynamicRows(aType, myAllValuesArray, aVersion);
			};
			cardbookElementTools.addEditButton(aHBox, aType, aIndex, 'down', 'down', fireDownButton);

			function fireRemoveButton(event) {
				if (document.getElementById(this.id).disabled) {
					return;
				}
				var myAllValuesArray = cardbookTypes.getAllTypes(aType, false);
				cardbookElementTools.deleteRowsType(aType);
				if (myAllValuesArray.length == 0) {
					cardbookTypes.constructDynamicRows(aType, myAllValuesArray, aVersion);
				} else {
					var removed = myAllValuesArray.splice(aIndex, 1);
					cardbookTypes.constructDynamicRows(aType, myAllValuesArray, aVersion);
				}
			};
			cardbookElementTools.addEditButton(aHBox, aType, aIndex, 'remove', 'remove', fireRemoveButton);
			
			function fireAddButton(event) {
				if (document.getElementById(this.id).disabled) {
					return;
				}
				var myValue = document.getElementById(aType + '_' + aIndex + '_valueBox').value;
				if (myValue == "") {                                                                                       
					return;
				}
				var myNextIndex = 1+ 1*aIndex;
				cardbookTypes.loadDynamicTypes(aType, myNextIndex, [], "", [], [""], aVersion);
			};
			cardbookElementTools.addEditButton(aHBox, aType, aIndex, 'add', 'add', fireAddButton);

			cardbookTypes.disableButtons(aType, aIndex);
		},

		loadDynamicEventsTypes: function (aType, aIndex, aEventType, aVersion) {
			var aOrigBox = document.getElementById(aType + 'Groupbox');
			
			if (aIndex == 0) {
				cardbookElementTools.addCaption(aType, aOrigBox);
			}
			
			var aHBox = cardbookElementTools.addHBox(aType, aIndex, aOrigBox);

			var aPrefButton = cardbookElementTools.addPrefStar(aHBox, aType, aIndex, aEventType[2])

			var aCalendarButton = document.createElement('button');
			aHBox.appendChild(aCalendarButton);
			aCalendarButton.setAttribute('id', aType + '_' + aIndex + 'CardbookCalendar');
			aCalendarButton.setAttribute('class', 'small-button cardbookCalendarButtonClass');
			function fireOpenCalendarButton(event) {
				wdw_cardEdition.openCalendarPanel(this);
			};
			aCalendarButton.addEventListener("click", fireOpenCalendarButton, false);
			aCalendarButton.addEventListener("command", fireOpenCalendarButton, false);

			cardbookElementTools.addTextbox(aHBox, aType + '_' + aIndex + '_valueDateBox', aEventType[0], {}, aIndex);
			cardbookElementTools.addKeyTextbox(aHBox, aType + '_' + aIndex + '_valueBox', aEventType[1], {flex: "1"}, aIndex);

			function fireUpButton(event) {
				if (document.getElementById(this.id).disabled) {
					return;
				}
				var myAllValuesArray = cardbookTypes.getAllEvents(false);
				if (myAllValuesArray.length <= 1) {
					return;
				}
				var temp = myAllValuesArray[aIndex*1-1];
				myAllValuesArray[aIndex*1-1] = myAllValuesArray[aIndex];
				myAllValuesArray[aIndex] = temp;
				cardbookElementTools.deleteRowsType(aType);
				cardbookTypes.constructDynamicEventsRows(aType, myAllValuesArray, aVersion);
			};
			cardbookElementTools.addEditButton(aHBox, aType, aIndex, 'up', 'up', fireUpButton);
			
			function fireDownButton(event) {
				if (document.getElementById(this.id).disabled) {
					return;
				}
				var myAllValuesArray = cardbookTypes.getAllEvents(false);
				if (myAllValuesArray.length <= 1) {
					return;
				}
				var temp = myAllValuesArray[aIndex*1+1];
				myAllValuesArray[aIndex*1+1] = myAllValuesArray[aIndex];
				myAllValuesArray[aIndex] = temp;
				cardbookElementTools.deleteRowsType(aType);
				cardbookTypes.constructDynamicEventsRows(aType, myAllValuesArray, aVersion);
			};
			cardbookElementTools.addEditButton(aHBox, aType, aIndex, 'down', 'down', fireDownButton);

			function fireRemoveButton(event) {
				if (document.getElementById(this.id).disabled) {
					return;
				}
				var myAllValuesArray = cardbookTypes.getAllEvents(false);
				cardbookElementTools.deleteRowsType(aType);
				if (myAllValuesArray.length == 0) {
					cardbookTypes.constructDynamicEventsRows(aType, myAllValuesArray, aVersion);
				} else {
					var removed = myAllValuesArray.splice(aIndex, 1);
					cardbookTypes.constructDynamicEventsRows(aType, myAllValuesArray, aVersion);
				}
			};
			cardbookElementTools.addEditButton(aHBox, aType, aIndex, 'remove', 'remove', fireRemoveButton);
			
			function fireAddButton(event) {
				if (document.getElementById(this.id).disabled) {
					return;
				}
				var myValue = document.getElementById(aType + '_' + aIndex + '_valueBox').value;
				if (myValue == "") {                                                                                       
					return;
				}
				var myNextIndex = 1+ 1*aIndex;
				cardbookTypes.loadDynamicEventsTypes(aType, myNextIndex, ["", ""], aVersion);
			};
			cardbookElementTools.addEditButton(aHBox, aType, aIndex, 'add', 'add', fireAddButton);

			cardbookTypes.disableButtons(aType, aIndex);
		},

		loadStaticTypes: function (aDirPrefId, aType, aIndex, aInputTypes, aPgName, aPgType, aCardValue, aVersion, aFollowLink) {
			if (aCardValue.join(" ") == "") {
				return;
			}

			if (aIndex == 0) {
				var aOrigBox = cardbookElementTools.addGroupbox(aType);
				cardbookElementTools.addCaption(aType, aOrigBox);
			} else {
				var panesView = cardbookPreferences.getStringPref("extensions.cardbook.panesView");
				var aOrigBox = document.getElementById(aType + panesView + 'Groupbox');
			}
			
			var aRow = cardbookElementTools.addGridRow(aOrigBox, aType + '_' + aIndex + '_gridRow', {align: 'start'});

			var myInputTypes = [];
			myInputTypes = cardbookUtils.getOnlyTypesFromTypes(aInputTypes);

			var myDisplayedTypes = [];
			if (aPgType.length != 0 && aPgName != "") {
				let found = false;
				for (var j = 0; j < aPgType.length; j++) {
					let tmpArray = aPgType[j].split(":");
					if (tmpArray[0] == "X-ABLABEL") {
						myDisplayedTypes.push(tmpArray[1]);
						found = true;
						break;
					}
				}
				if (!found) {
					myDisplayedTypes.push(cardbookTypes.whichLabelTypeShouldBeChecked(aType, aDirPrefId, myInputTypes));
				}
			} else {
				myDisplayedTypes.push(cardbookTypes.whichLabelTypeShouldBeChecked(aType, aDirPrefId, myInputTypes));
			}
			
			var aPrefImage = document.createElement('image');
			aRow.appendChild(aPrefImage);
			aPrefImage.setAttribute('id', aType + '_' + aIndex + '_PrefImage');
			if (cardbookUtils.getPrefBooleanFromTypes(aInputTypes)) {
				aPrefImage.setAttribute('class', 'cardbookPrefStarClass');
				aPrefImage.setAttribute('haspref', 'true');
			} else {
				aPrefImage.setAttribute('class', 'cardbookNoPrefStarClass');
				aPrefImage.removeAttribute('haspref');
			}

			var myContextMenu = '';
			if (aFollowLink) {
				myContextMenu = aType + 'TreeContextMenu';
			}
			cardbookElementTools.addTextbox(aRow, aType + '_' + aIndex + '_prefWeightBox', cardbookUtils.getPrefValueFromTypes(aInputTypes, document.getElementById('versionTextBox').value),
										{readonly: 'true'});
			if (document.getElementById('versionTextBox').value === "4.0") {
				document.getElementById(aType + '_' + aIndex + '_prefWeightBox').setAttribute('hidden', 'false');
				document.getElementById(aType + '_' + aIndex + '_prefWeightBox').setAttribute('width', '3');
			} else {
				document.getElementById(aType + '_' + aIndex + '_prefWeightBox').setAttribute('hidden', 'true');
			}

			var myValueTextbox;
			if (aType == "impp") {
				var serviceCode = cardbookTypes.getIMPPCode(aInputTypes);
				var serviceProtocol = cardbookTypes.getIMPPProtocol(aCardValue);
				var myValue = aCardValue.join(" ");
				if (serviceCode != "") {
					var serviceLine = [];
					serviceLine = cardbookTypes.getIMPPLineForCode(serviceCode)
					if (serviceLine[0]) {
						myDisplayedTypes = myDisplayedTypes.concat(serviceLine[1]);
						cardbookElementTools.addTextbox(aRow, aType + '_' + aIndex + '_typeBox', cardbookUtils.formatTypesForDisplay(myDisplayedTypes), {readonly: 'true'});
						var myRegexp = new RegExp("^" + serviceLine[2] + ":");
						myValue = myValue.replace(myRegexp, "");
						myValueTextbox = cardbookElementTools.addTextbox(aRow, aType + '_' + aIndex + '_valueBox', myValue, {context: myContextMenu, flex: '1'});
						myValueTextbox.setAttribute('link', 'true');
					} else {
						myDisplayedTypes = myDisplayedTypes.concat(serviceCode);
						cardbookElementTools.addTextbox(aRow, aType + '_' + aIndex + '_typeBox', cardbookUtils.formatTypesForDisplay(myDisplayedTypes), {readonly: 'true'});
						myValueTextbox = cardbookElementTools.addTextbox(aRow, aType + '_' + aIndex + '_valueBox', myValue, {context: myContextMenu, flex: '1'});
						myValueTextbox.setAttribute('readonly', 'true');
					}
				} else if (serviceProtocol != "") {
					var serviceLine = [];
					serviceLine = cardbookTypes.getIMPPLineForProtocol(serviceProtocol)
					if (serviceLine[0]) {
						myDisplayedTypes = myDisplayedTypes.concat(serviceLine[1]);
						cardbookElementTools.addTextbox(aRow, aType + '_' + aIndex + '_typeBox', cardbookUtils.formatTypesForDisplay(myDisplayedTypes), {readonly: 'true'});
						var myRegexp = new RegExp("^" + serviceLine[2] + ":");
						myValue = myValue.replace(myRegexp, "");
						myValueTextbox = cardbookElementTools.addTextbox(aRow, aType + '_' + aIndex + '_valueBox', myValue, {context: myContextMenu, flex: '1'});
						myValueTextbox.setAttribute('link', 'true');
					} else {
						myDisplayedTypes = myDisplayedTypes.concat(serviceCode);
						cardbookElementTools.addTextbox(aRow, aType + '_' + aIndex + '_typeBox', cardbookUtils.formatTypesForDisplay(myDisplayedTypes), {readonly: 'true'});
						myValueTextbox = cardbookElementTools.addTextbox(aRow, aType + '_' + aIndex + '_valueBox', myValue, {context: myContextMenu, flex: '1'});
						myValueTextbox.setAttribute('readonly', 'true');
					}
				} else {
					cardbookElementTools.addTextbox(aRow, aType + '_' + aIndex + '_typeBox', cardbookUtils.formatTypesForDisplay(myDisplayedTypes), {readonly: 'true'});
					myValueTextbox = cardbookElementTools.addTextbox(aRow, aType + '_' + aIndex + '_valueBox', myValue, {context: myContextMenu, flex: '1'});
					myValueTextbox.setAttribute('readonly', 'true');
				}
			} else {
				cardbookElementTools.addTextbox(aRow, aType + '_' + aIndex + '_typeBox', cardbookUtils.formatTypesForDisplay(myDisplayedTypes), {readonly: 'true'});
	
				if (aType == "adr") {
					var re = /[\n\u0085\u2028\u2029]|\r\n?/;
					var myAdrResult = cardbookUtils.formatAddress(aCardValue);
					var myAdrResultArray = myAdrResult.split(re);
					myValueTextbox = cardbookElementTools.addTextbox(aRow, aType + '_' + aIndex + '_valueBox', myAdrResult, {context: myContextMenu,
																								multiline: 'true', wrap: 'virtual', rows: myAdrResultArray.length});
				} else {
					myValueTextbox = cardbookElementTools.addTextbox(aRow, aType + '_' + aIndex + '_valueBox', cardbookUtils.cleanArray(aCardValue).join(" "), {context: myContextMenu, flex: '1'});
				}
				if (aType == "url" || aType == "email" || aType == "adr") {
					myValueTextbox.setAttribute('link', 'true');
				} else if (aType == "tel") {
					var telProtocol = "";
					try {
						var telProtocol = cardbookPreferences.getStringPref("extensions.cardbook.tels.0");
						myValueTextbox.setAttribute('link', 'true');
					}
					catch(e) {
						myValueTextbox.setAttribute('readonly', 'true');
					}
				}
			}
			if (aFollowLink) {
				function fireClick(event) {
					if (wdw_cardbook) {
						wdw_cardbook.chooseActionTreeForClick(event)
					}
				};
				myValueTextbox.addEventListener("click", fireClick, false);
			}
		},

		loadStaticEventsTypes: function (aDirPrefId, aType, aIndex, aEventType, aVersion) {
			if (aIndex == 0) {
				var aOrigBox = cardbookElementTools.addGroupbox(aType);
				cardbookElementTools.addCaption(aType, aOrigBox);
			} else {
				var panesView = cardbookPreferences.getStringPref("extensions.cardbook.panesView");
				var aOrigBox = document.getElementById(aType + panesView + 'Groupbox');
			}
			
			var aRow = cardbookElementTools.addGridRow(aOrigBox, aType + '_' + aIndex + '_gridRow', {align: 'start'});

			var aPrefImage = document.createElement('image');
			aRow.appendChild(aPrefImage);
			aPrefImage.setAttribute('id', aType + '_' + aIndex + '_PrefImage');
			if (aEventType[2]) {
				aPrefImage.setAttribute('class', 'cardbookPrefStarClass');
				aPrefImage.setAttribute('haspref', 'true');
			} else {
				aPrefImage.setAttribute('class', 'cardbookNoPrefStarClass');
				aPrefImage.removeAttribute('haspref');
			}

			cardbookElementTools.addTextbox(aRow, aType + '_' + aIndex + '_prefWeightBox', '', {readonly: 'true'});
			if (aVersion === "4.0") {
				document.getElementById(aType + '_' + aIndex + '_prefWeightBox').setAttribute('hidden', 'false');
				document.getElementById(aType + '_' + aIndex + '_prefWeightBox').setAttribute('width', '3');
			} else {
				document.getElementById(aType + '_' + aIndex + '_prefWeightBox').setAttribute('hidden', 'true');
			}
			
			var dateFormat = cardbookPreferences.getDateFormat(aDirPrefId);
			var myDate = cardbookDates.getFormattedDateForDateString(aEventType[0], dateFormat, cardbookRepository.dateDisplayedFormat);
			cardbookElementTools.addTextbox(aRow, aType + '_' + aIndex + '_typeBox', myDate, {readonly: 'true'});
	
			cardbookElementTools.addTextbox(aRow, aType + '_' + aIndex + '_valueBox', aEventType[1], {readonly: 'true', flex: '1'});
		},

		loadMailPopularity: function (aCard, aReadOnly) {
			var myEmails = [];
			if (aCard.isAList) {
				myEmails.push(aCard.fn.toLowerCase());
			} else {
				for (var i = 0; i < aCard.email.length; i++) {
					myEmails.push(aCard.email[i][0][0].toLowerCase());
				}
			}

			for (var i = 0; i < myEmails.length; i++) {
				var aOrigBox = document.getElementById('mailPopularityGroupbox');

				if (i == 0) {
					cardbookElementTools.addCaption('mailPopularity', aOrigBox);
				}

				var aRow = document.createElement('row');
				aOrigBox.appendChild(aRow);
				aRow.setAttribute('id', 'mailPopularity_' + i + '_row');
				aRow.setAttribute('flex', '1');
				aRow.setAttribute('align', 'center');

				if (aReadOnly) {
					var aImage = document.createElement('image');
					aRow.appendChild(aImage);
					aImage.setAttribute('id', 'dummyMailPopularityPrefBox_' + i);
					aImage.setAttribute('class', 'cardbookNoPrefStarClass');
				}

				if (cardbookRepository.cardbookMailPopularityIndex[myEmails[i]]) {
					var mailPopularityValue = cardbookRepository.cardbookMailPopularityIndex[myEmails[i]];
				} else {
					var mailPopularityValue = "";
				}
				if (aReadOnly) {
					cardbookElementTools.addTextbox(aRow, 'popularity_' + i + '_Textbox', mailPopularityValue, {flex: '1', readonly: 'true'});
					cardbookElementTools.addTextbox(aRow, 'email_' + i + '_Textbox', myEmails[i], {flex: '1', readonly: 'true'});
				} else {
					cardbookElementTools.addTextbox(aRow, 'popularity_' + i + '_Textbox', mailPopularityValue, {flex: '1'});
					cardbookElementTools.addTextbox(aRow, 'email_' + i + '_Textbox', myEmails[i], {flex: '1'});
				}
			}
		},

		loadStaticList: function (aCard, aFollowLink) {
			var addedCards = [];
			if (aCard.version == "4.0") {
				for (var i = 0; i < aCard.member.length; i++) {
					if (aCard.member[i].startsWith("mailto:")) {
						var email = aCard.member[i].replace("mailto:", "");
						addedCards.push(["", [email.toLowerCase()], ""]);
					} else {
						var uid = aCard.member[i].replace("urn:uuid:", "");
						if (cardbookRepository.cardbookCards[aCard.dirPrefId+"::"+uid]) {
							var cardFound = cardbookRepository.cardbookCards[aCard.dirPrefId+"::"+uid];
							if (cardFound.isAList) {
								addedCards.push([cardbookUtils.getName(cardFound), [""], cardFound.dirPrefId+"::"+cardFound.uid]);
							} else {
								addedCards.push([cardbookUtils.getName(cardFound), cardFound.emails, cardFound.dirPrefId+"::"+cardFound.uid]);
							}
						}
					}
				}
			} else if (aCard.version == "3.0") {
				var kindCustom = cardbookPreferences.getStringPref("extensions.cardbook.kindCustom");
				var memberCustom = cardbookPreferences.getStringPref("extensions.cardbook.memberCustom");
				for (var i = 0; i < aCard.others.length; i++) {
					var localDelim1 = aCard.others[i].indexOf(":",0);
					if (localDelim1 >= 0) {
						var header = aCard.others[i].substr(0,localDelim1);
						var trailer = aCard.others[i].substr(localDelim1+1,aCard.others[i].length);
						if (header == memberCustom) {
							if (trailer.startsWith("mailto:")) {
								var email = trailer.replace("mailto:", "");
								addedCards.push(["", [email.toLowerCase()], ""]);
							} else {
								if (cardbookRepository.cardbookCards[aCard.dirPrefId+"::"+trailer.replace("urn:uuid:", "")]) {
									var cardFound = cardbookRepository.cardbookCards[aCard.dirPrefId+"::"+trailer.replace("urn:uuid:", "")];
									if (cardFound.isAList) {
										addedCards.push([cardbookUtils.getName(cardFound), [""], cardFound.dirPrefId+"::"+cardFound.uid]);
									} else {
										addedCards.push([cardbookUtils.getName(cardFound), cardFound.emails, cardFound.dirPrefId+"::"+cardFound.uid]);
									}
								}
							}
						}
					}
				}
			}

			for (var i = 0; i < addedCards.length; i++) {
				var aOrigBox = document.getElementById('addedCardsGroupbox');

				if (i == 0) {
					cardbookElementTools.addCaption('addedCards', aOrigBox);
				}

				var aRow = document.createElement('row');
				aOrigBox.appendChild(aRow);
				aRow.setAttribute('id', 'addedCards_' + i + '_row');
				aRow.setAttribute('flex', '1');
				aRow.setAttribute('align', 'center');

				var aImage = document.createElement('image');
				aRow.appendChild(aImage);
				aImage.setAttribute('id', 'dummyListPrefBox_' + i);
				aImage.setAttribute('class', 'cardbookNoPrefStarClass');

				cardbookElementTools.addTextbox(aRow, 'email_' + addedCards[i][2] + '_valueBox', addedCards[i][1].join(" "), {flex: '1', readonly: 'true'});

				var myCardTextbox = cardbookElementTools.addTextbox(aRow, 'fn_' + addedCards[i][2] + '_valueBox', addedCards[i][0], {context: 'listsContextMenu', flex: '1', readonly: 'true'});
				if (aFollowLink) {
					myCardTextbox.setAttribute('link', 'true');
					function fireClick(event) {
						if (wdw_cardbook) {
							wdw_cardbook.chooseActionTreeForClick(event)
						}
					};
					myCardTextbox.addEventListener("click", fireClick, false);
				}

			}
		}

	};

};
